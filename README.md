# tirl
Tensor Image Registration Library


If you are installing TIRL from source, you may create a separate
enviroment for your installation using the provided tirlenv.yml environment
descriptor file.

## FSL Installation

TIRL may be already installed on your system as part of FSL. To use this
installation, activate the ```fslpython``` conda environment:

```shell script
source $FSLDIR/fslpython/bin/activate fslpython
```

The TIRL executable should now be accessible by typing ```tirl``` in your
terminal:

```shell script
tirl
```

## Installation from source

If you would like to use a more recent version of TIRL than what is installed
with FSL, you can install the library from source. It is recommended that you
create a separate conda environment for this purpose, as outlined below.

1. Download the TIRL source code.

```shell script
mkdir -p ~/Applications/
cd ~/Applications
git clone https://git.fmrib.ox.ac.uk/ihuszar/tirl.git
cd tirl
```

2. Create virtual environment for registration projects with TIRL.

```shell script
conda env create -n tirlenv -f tirlenv.yml
```

3. Activate the new tirlenv environment.

```shell script
conda activate tirlenv
```

4. Install TIRL from its source directory.

```shell script
pip install .
```

5. On some machines, the installer may not find the openBLAS headers in the conda environment automatically, and the installer will raise an error that "lapacke.h" was not found. If this happens to you, configure this environment variable, that will allow the gcc compiler to find the required header files. Type the following in your terminal while you are within tirlenv. You must deactivate and reactivate the environment after this step.

```shell script
conda env config vars set C_INCLUDE_PATH=${CONDA_PREFIX}/include:${C_INCLUDE_PATH}
```

If the variable is set, follow step 4 to install TIRL.

## Configuration

Note: to configure TIRL, you must be within the Python environment where you
installed TIRL ('tirlenv' in the above example).

To configure TIRL for your system, type ```tirl config``` in your terminal
window. This will allow you to edit the configuration parameters using vim.

To use another editor, e.g. nano, type ```tirl config nano```. Other options
might include subl (Sublime Text), atom (Atom), or emacs (Emacs), depending on
what is available on your system.

Before you start using TIRL, you must make sure that the **temporary working
directory (TWD)** is set correctly. You should have write access to this
folder, and ideally it should be on a fast-access storage device such as an
NVME SSD. TIRL will use this folder to deposit large files to avoid overloading
the system memory. Files will normally disappear from this folder after a tirl
script finishes, but if you notice that the files in this folder occupy a lot
of disk space, you should delete them manually.

## Basic test

1. Open the python shell by typing ```python``` in the terminal window (make
sure that you have previously activated the 'tirlenv' environment).

2. Type the following in the interactive shell:

```python
import tirl
img = tirl.testimg()
img.preview()
```

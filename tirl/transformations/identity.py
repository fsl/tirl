#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from numbers import Integral


# TIRL IMPORTS

from tirl import utils as tu
from tirl.transformations import Transformation


# IMPLEMENTATION

class TxIdentity(Transformation):
    """
    TxIdentity class - identity transformation and conversion between
    homogeneous and non-homogeneous coordinates.

    """

    RESERVED_KWARGS = ("parameters", "homogenise", "name", "invertible",
                       "metaparameters")

    def __init__(self, homogenise=0, name=None, invertible=True,
                 **metaparameters):
        """
        Initialisation of TxIdentity. This transformation does not have
        parameters, only metaparameters.

        :param homogenise:
            Use this switch to control the output:
               >0: add homogeneous part,
               =0: leave input coordinates as they are,
               <0: remove homogeneous part
        :type homogenise: int
                :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param metaparameters:
            Metaparameters for TxIdentity.
        :type metaparameters: Any

        """
        # Call superclass initialisation
        super(TxIdentity, self).__init__(
            bounds=None, lock=None, name=name, invertible=invertible,
            **metaparameters)

        # Set class-specific metaparameters
        self.homogenise = homogenise

    @classmethod
    def _load(cls, dump):
        meta = dump.get("metaparameters")
        obj = cls(**meta)
        return obj

    @property
    def dim(self):
        """
        Returns the number of coordinates expected by the transformation.

        """
        # Unspecified
        return None

    @property
    def homogenise(self):
        return self.metaparameters.get("homogenise")

    @homogenise.setter
    def homogenise(self, h):
        if isinstance(h, (bool, np.bool_, Integral)):
            h = int(h) // abs(h) if h != 0 else 0
            self.metaparameters.update({"homogenise": h})
        else:
            raise TypeError(f"Expected boolean/integer for homogenise option, "
                            f"got {h.__class__.__name__} instead.")

    def map(self, coords, signature=None):
        """
        Applies transformation to input coordinates.

        :param coords: (n_points, n_dimensions) table of input coordinates
        :type coords: Union[np.ndarray, tuple, list]
        :param signature: Signature of the input coordinates.
        :type signature: list

        :returns: (n_points, m_coordinates) table of transformed coordinates
        :rtype: np.ndarray
        """
        coords = super(TxIdentity, self).map(coords, signature)

        if self.homogenise == 0:
            return coords

        elif self.homogenise > 0:
            n, m = coords.shape
            newcoords = np.ones(shape=(n, m + 1), dtype=coords.dtype)
            newcoords[:, :-1] = coords
            return newcoords

        else:
            n, m = coords.shape
            newcoords = np.delete(coords, obj=m - 1, axis=1)
            return newcoords

    def map_vector(self, vects, coords=None, rule=None, signature=None):
        """
        Applies transformation to vectors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point.

        """
        # Mapping is identical to that of the coordinates, which are
        # essentially position vectors.
        return self.map(vects)

    def map_tensor(self, tensors, coords=None, rule=None, signature=None):
        """
        Applies transformation to tensors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point.

        """
        tensors = super(TxIdentity, self).map_tensor(
            tensors, coords=coords, rule=rule, signature=signature)

        if self.homogenise == 0:
            return tensors

        assert tensors.shape[-2] == tensors.shape[-1]
        array_shape = tensors.shape[:-2]
        r = tensors.shape[-1]

        if self.homogenise > 0:
            new_tensors = \
                np.zeros(array_shape + (r + 1, r + 1), dtype=tensors.dtype)
            new_tensors[..., :r, :r] = tensors
            new_tensors[..., -1, -1] = 1

        else:
            new_tensors = tensors[..., :-1, :-1]

        return new_tensors

    def map_vector_and_coordinates(
            self, vects, coords=None, rule=None, signature=None):
        """
        Applies transformation to vectors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point. Also returns the transformed coordinates.

        """
        vects = self.map_vector(vects, coords, rule, signature)
        if coords is not None:
            coords = self.map(coords)
        return vects, coords

    def map_tensor_and_coordinates(
            self, tensors, coords=None, rule=None, signature=None):
        """
        Applies transformation to tensors at the locations specified by the
        coordinates. As linear transformations are location-invariant,
        coordinates are optional for linear transformations, but obligatory for
        non-linear transformations, where the transformation may vary from
        point to point. Also returns the transformed coordinates.

        """
        tensors = self.map_tensor(tensors, coords, rule, signature)
        if coords is not None:
            coords = self.map(coords)
        return tensors, coords

    def inverse(self):
        """
        Creates inverse transformation object.

        :returns: TxIdentity with inverted 'homogenise' option.
        :rtype: TxIdentity

        """
        meta = tu.rcopy(self.metaparameters)
        homogenise = meta.pop("homogenise")
        name = meta.pop("name")
        name = f"{name}_inv"
        invertible = meta.pop("invertible")
        obj = type(self)(-homogenise, name=name, invertible=invertible, **meta)
        return obj


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")

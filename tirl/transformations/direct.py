#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from numbers import Integral


# TIRL IMPORTS

from tirl.transformations import Transformation
from tirl.transformations.identity import TxIdentity


# IMPLEMENTATION

class TxDirect(Transformation):
    """
    TxDirect class - implements direct assignment between voxel and physical
    coordinates. TxDirect maps integer indices to n-dimensional vectors.
    Vectors are stored as the parameters of the transformation as a (C-order)
    flattened (n_points, m_dimensions) array.

    Possible use cases:
        - define fractional voxel coordinates
        - define multi-dimensional point-set (non-compact) domains, where the
          actual voxel array is 1-dimensional

    """
    RESERVED_KWARGS = ("parameters", "bounds", "lock", "name", "invertible",
                       "metaparameters")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, coordinates, bounds=None, lock=None, name=None,
                 invertible=False, **metaparameters):
        """
        Initialisation of TxDirect.

        :param coordinates:
            (n_points, m_dimensions) array of physical coordinates that will
            be assigned to voxel coordinates in the order they are specified
            along the 0-th axis.
        :type coordinates: np.ndarray or np.memmap
        :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param kwargs: additional keyword arguments for TxDirect
        :type kwargs: Any

        """
        n_points, ndim = coordinates.shape

        # Call superclass initialisation
        super(TxDirect, self).__init__(
            coordinates.ravel(), bounds=bounds, lock=lock, name=name,
            invertible=invertible, **metaparameters)

        # Set class-specific attributes
        self.ndim = ndim

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def ndim(self):
        return self.metaparameters.get("ndim")

    @ndim.setter
    def ndim(self, value):
        if isinstance(value, Integral):
            self.metaparameters.update(ndim=int(value))
        else:
            raise TypeError(f"Invalid number of physical dimensions: {value}.")

    def coordinates(self):
        return self.parameters.parameters.reshape(-1, self.ndim)

    def map(self, coords, signature=None):
        indices = coords[:, 0].ravel().astype(int)
        assert np.allclose(indices, coords[:, 0].ravel())
        return self.coordinates()[indices]

    def map_vector(self, vects, coords=None, rule=None, signature=None):
        # This is a pass-through. TxDirect does not change the orientation of
        # the vectors.
        return super(TxDirect, self).map_vector(
            vects, coords=coords, rule=rule, signature=signature)

    def map_tensor(self, tensors, coords=None, rule=None, signature=None):
        # This is a pass-through. TxDirect does not change the orientation of
        # the tensors.
        return super(TxDirect, self).map_tensor(
            tensors, coords=coords, rule=rule, signature=signature)

    def map_vector_and_coordinates(
            self, vects, coords=None, rule=None, signature=None):
        # This is a pass-through. TxDirect does not change the orientation of
        # the vectors.
        if coords is not None:
            coords = self.map(coords)
        vects = self.map_vector(
            vects, coords=coords, rule=rule, signature=signature)
        return vects, coords

    def map_tensor_and_coordinates(
            self, tensors, coords=None, rule=None, signature=None):
        # This is a pass-through. TxDirect does not change the orientation of
        # the tensors.
        if coords is not None:
            coords = self.map(coords)
        tensors = self.map_tensor(
            tensors, coords=coords, rule=rule, signature=signature)
        return tensors, coords

    def inverse(self):
        """ Returns a TxDirect instance that maps the input coordinates to
        an equal number of consecutive points in a 1D voxel array. """
        return TxIdentity()
        # coordinates = np.arange(self.parameters.size / self.ndim).reshape(-1, 1)
        # name = f"{self.name}_inv"
        # return type(self)(coordinates, bounds=None, lock=None, name=name,
        #                   invertible=False)

    @classmethod
    def _load(cls, dump):
        params = dump.pop("parameters")
        meta = dump.pop("metaparameters")
        ndim = meta.pop("ndim")
        coords = params.parameters.reshape(-1, ndim)
        lb = params.lower_bounds
        ub = params.upper_bounds
        return cls(coords, bounds=(lb, ub), **meta)




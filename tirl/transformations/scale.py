#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from numbers import Integral


# TIRL IMPORTS

from tirl import utils as tu, settings as ts
from tirl.transformations.linear import TxLinear


# IMPLEMENTATION

class TxScale(TxLinear):
    """
    TxScale (subclass of TxLinear) - multiplies coordinates with a scalar
    factor.

    Negative numbers and zero are both allowed as factors to achieve
    reflection and orthogonal projection. Note that orthogonal projection is
    not invertible!

    """

    RESERVED_KWARGS = ("parameters", "bounds", "lock", "name", "invertible",
                       "metaparameters")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, *parameters, bounds=None, lock=None, name=None,
                 invertible=True, **metaparameters):
        """
        Initialisation of TxScale.

        :param parameters:
            Scale factors for each coordinate dimension. Negative scale factors
            imply reflection along the corresponding axis. A factor of 0
            implies projection onto the plane that is orthogonal to the axis
            with the 0 scale.
        :type parameters: Union[float, int]
        :param bounds:
            Parameter bounds can be defined in one of the following formats:
            1. ((lb0, ub0),) - sequence of lower and upper bounds for each
               parameter. The order must correspond to the order of the
               transformation parameters.
            2. {param_no: (lb, ub)} - dictionary of lower and upper bound
               pairs, where the keys represent parameter indices (starting
               from 0).
            3. (lb_arr, ub_arr) - flat iterable with lower and upper bounds for
               all parameters.
            4. "auto" - the get_default_bounds() method is called to create
               default lower and upper bounds.
            5. None - parameter bounds are not set.
            6. (lb, ub) - global scalar values for lower and upper bounds
        :type bounds: Union[tuple, list, dict, str]
        :param lock:
            Boolean mask or index sequence that defines which parameters
            are locked, i.e. should not be updated by default. If None, all
            parameters are updated by default when calling the update() method.
        :type lock: Union[None, tuple, int, np.ndarray]
        :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param metaparameters:
            Metaparameters for Transformation
        :type metaparameters: Any

        """
        if parameters == ():
            raise ValueError("No parameters were specified.")

        # Interpret the parameter and shape inputs. For this class the shape
        # input is None, as it can be inferred from the number of parameters.
        parameters, shape = \
            self._read_parameters_and_shape(parameters, shape=None)
        shape = (parameters.size, parameters.size)
        
        # Call the superclass initialisation
        super(TxScale, self).__init__(
            *parameters, shape=shape, bounds=bounds, lock=lock, name=name,
            invertible=invertible, **metaparameters)

        # Set class-specific metaparameters
        # Nothing to do.

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @property
    def dim(self):
        """
        Returns the number of cooridinates expected by the transformation.

        """
        return self.parameters.size

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @classmethod
    def _load(cls, dump):
        parameters = dump.get("parameters")
        lb = parameters.lower_bounds
        ub = parameters.upper_bounds
        lock = parameters.locked
        meta = dump.get("metaparameters")
        name = meta.pop("name")
        shape = meta.pop("shape")
        invertible = meta.pop("invertible")
        obj = cls(*parameters[:], bounds=(lb, ub), lock=lock, name=name,
                  invertible=invertible, **meta)
        return obj

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @staticmethod
    def matrix2params(mat, **kwargs):
        """
        Creates parameter vector from scale matrix.

        :param mat: scale matrix
        :type mat: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve parameters from
            transformation matrix.
        :type kwargs: Any

        :returns: scale parameters
        :rtype: np.ndarray

        """
        mat = np.asarray(mat, dtype=ts.DEFAULT_PARAMETER_DTYPE)
        return np.diag(mat)

    @staticmethod
    def params2matrix(parameters, **kwargs):
        """
        Creates scale matrix from scale parameters.

        :param parameters: scale parameters
        :type parameters: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve matrix from parameters.
        :type kwargs: Any

        """
        parameters = np.asarray(parameters, dtype=ts.DEFAULT_PARAMETER_DTYPE)
        mat = np.diag(parameters)
        return mat

    def map(self, coords, signature=None):
        """
        Scales input coordinates by the transformation parameters. Coordinate
        dimensions without a corresponding scale factor will be left unchanged.
        If there are more scaling factors than input coordinates, an exception
        is raised.

        :param coords: (n_points, n_dimensions) table of input coordinates
        :type coords: np.ndarray
        :param signature: signature of the input coordinates
        :type signature: list

        :raises:
            IndexError if there are more input coordinates than scale factors.

        :returns: (n_points, m_dimensions) table of transformed coordinates
        :rtype: np.ndarray

        """
        # Call the map method of the Transformation base class
        coords = super(TxLinear, self).map(coords, signature)

        ns = self.parameters.size
        nc = coords.shape[-1]
        if ns > nc:
            raise IndexError(f"There are more scale factors than input "
                             f"coordinates ({nc}).")
        factors = np.ones(nc, dtype=coords.dtype)
        factors[:ns] = self.parameters[:]
        return coords * factors

    def inverse(self):
        """
        Creates inverse transformation object by inverting the scale matrix. If
        one or more of the scale factors is zero, the transformation is an
        orthogonal projection, which does not have an inverse, because it is
        not a bijection.

        :raises: ArithmeticError if zero is among in the parameters

        :returns: inverse transformation object
        :rtype: TxScale

        """
        try:
            invmat = np.linalg.inv(self.matrix)
        except np.linalg.LinAlgError as exc:
            raise ArithmeticError("Scale matrix is not invertible.")
        params = np.diag(invmat)
        meta = tu.rcopy(self.metaparameters)
        name = meta.pop("name")
        name = f"{name}_inv"
        shape = meta.pop("shape")
        invertible = meta.pop("invertible")
        obj = type(self)(*params, bounds=None, lock=None, name=name,
                         invertible=invertible, **meta)
        return obj

  # TODO: Deprecate Transformation.jacobian()
    def jacobian(self, coords):
        coords = super(TxLinear, self).map(coords)
        n_points, n_coords = coords.shape
        n_dim = self.parameters.size
        # Formalise the parameter axis in the Jacobian
        diag = (1,) * n_dim + (0,) * (n_coords - n_dim)
        I1 = np.diag(diag)[:n_dim]
        I2 = np.diag(diag)
        jac = np.einsum("kj,lj,ij", I1, I2, coords)
        return jac


class TxIsoScale(TxLinear):

    def __init__(self, factor, dim, bounds=None, lock=None, name=None,
                 invertible=True, **metaparameters):

        if not isinstance(dim, Integral) or (dim <= 0):
            raise TypeError(f"Expected a positive integer for the number of "
                            f"dimensions, got {dim} instead.")

        # Interpret the parameter and shape inputs. For this class the shape
        # input is None, as it can be inferred from 'dim'.
        parameters, shape = \
            self._read_parameters_and_shape([factor], shape=None)

        # Call the superclass initialisation
        super(TxIsoScale, self).__init__(
            *parameters, shape=(dim, dim), bounds=bounds, lock=lock, name=name,
            invertible=invertible, **metaparameters)

        # Set class-specific metaparameters
        self.metaparameters.update(shape=(dim, dim))

    @staticmethod
    def matrix2params(mat, **kwargs):
        assert mat.shape == 2, "Input matrix is not a matrix."
        assert mat.shape[0] == mat.shape[1], "Scale matrix must be square."
        params = np.diagonal(mat).astype(ts.DEFAULT_PARAMETER_DTYPE)
        assert np.allclose(params, params[0],
                           atol=np.finfo(params.dtype).eps), \
            "Scale parameters are not identical across dimensions."
        return params[0]

    @staticmethod
    def params2matrix(parameters, **kwargs):
        parameters = np.asarray(parameters)
        if parameters.size != 1:
            raise ValueError(f"Expected 1 parameter, got {parameters.size} "
                             f"instead.")
        dim = kwargs.get("shape")[0]
        return np.diag([parameters[0]] * dim)

    def map(self, coords, signature=None):
        coords = super(TxLinear, self).map(coords, signature)
        ns = self.dim
        nc = coords.shape[-1]
        if ns > nc:
            raise IndexError(f"There are more scale factors than input "
                             f"coordinates ({nc}).")
        factors = np.ones(nc, dtype=coords.dtype)
        factors[:ns] *= self.parameters[0]
        return coords * factors

    def inverse(self):
        factor = self.parameters[0]
        name = self.name + "_inv"
        meta = {k: v for k, v in self.metaparameters.items()
                if k not in ("factor", "dim", "bounds", "lock", "name",
                             "invertible", "metaparameters", "shape")}
        obj = type(self)(1. / factor, dim=self.dim, bounds=None, lock=None,
                         name=name, invertible=self.invertible, **meta)
        return obj

    @classmethod
    def _load(cls, dump):
        parameters = dump.get("parameters")
        lb = parameters.lower_bounds
        ub = parameters.upper_bounds
        lock = parameters.locked
        meta = dump.get("metaparameters")
        dim = meta.pop("shape")[0]
        name = meta.pop("name")
        invertible = meta.pop("invertible")
        return cls(parameters[0], dim, bounds=(lb, ub), lock=lock, name=name,
                   invertible=invertible, **meta)


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")

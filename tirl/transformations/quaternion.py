#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from numbers import Real


# TIRL IMPORTS

import tirl.utils as tu
from tirl import settings as ts
from tirl.beta import BetaClassMeta

from tirl.point import Point
from tirl.transformations.rotation import TxRotation3D


# IMPLEMENTATION

# TODO: Further testing required.
class TxQuaternion(TxRotation3D, metaclass=BetaClassMeta):
    """
    TxQuaternion (child of TxRotation3D) - 3D rotation object that uses the
    quaternion parametrisation.

    The quaternion representation is immune to the gimbal lock condition that
    may be encountered when optimising rotation angles. As a trade-off,
    quaternion rotations are non-intuitive.

    This class works with unit quaternions, hence the square-sum of the
    components mut be 1 at all times. To enforce this requirement, the class
    default for parameter bounds is [-1, 1]. Furthermore, the input values for
    the quaternion components are normalised during initialisation and updating
    by the set and update methods. Normalisation does not take place when
    parameter values are manipulated directly through the self.parameters
    interface, therefore this must be avoided to prevent erroneous calculation
    of the rotation matrix.

    Since quaternion normalisation is built into the initialisation, set and
    update methods, changing one parameter will also affect other parameters,
    regardless whether they are locked or free.

    """

    RESERVED_KWARGS = ("quaternion", "bounds", "lock", "name", "invertible",
                       "metaparameters")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~ INITIALISATION ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    def __init__(self, *quaternion, centre=None, bounds=(-1, 1), lock=None,
                 name=None, invertible=True, **metaparameters):
        """
        Initialisation of TxRotationQuaternion.

        :param quaternion:
            Four real numbers that define the rotation quaternion.
        :type quaternion: Union[int, float]
        :param bounds:
            Parameter bounds can be defined in one of the following formats:
            1. ((lb0, ub0),) - sequence of lower and upper bounds for each
               parameter. The order must correspond to the order of the
               transformation parameters.
            2. {param_no: (lb, ub)} - dictionary of lower and upper bound
               pairs, where the keys represent parameter indices (starting
               from 0).
            3. (lb_arr, ub_arr) - flat iterable with lower and upper bounds for
               all parameters.
            4. "auto" - the get_default_bounds() method is called to create
               default lower and upper bounds.
            5. None - parameter bounds are not set.
            6. (lb, ub) - global scalar values for lower and upper bounds
        :type bounds: Union[tuple, list, dict, str]
        :param lock:
            Boolean mask or index sequence that defines which parameters
            are locked, i.e. should not be updated by default. If None, all
            parameters are updated by default when calling the update() method.
        :type lock: Union[None, tuple, int, np.ndarray]
        :param name:
            Name of the Transformation object. If None (default), a default
            name of the form "tx_objectID" will be generated.
        :type name: str
        :param invertible:
            Indicates whether the current transformation is invertible (True) or
            not (False, default). Transformations marked 'invertible' must
            implement the inverse() method.
        :type invertible: bool
        :param metaparameters:
            Metaparameters for TxQuaternion.
        :type metaparameters: Any

        """
        # Interpret the parameter input. The shape is (3, 3 or 4) for this class
        shape = (3, 3) if centre is None else (3, 4)
        parameters, shape = \
            self._read_parameters_and_shape(quaternion, shape=shape)

        # Enforce quaternion structure
        try:
            parameters = parameters / np.linalg.norm(parameters)
            assert all(isinstance(q, Real) for q in parameters)
        except:
            raise ValueError("Invalid quaternion specification.")

        # Call superclass initialisation
        super(TxQuaternion, self).__init__(
            *parameters, centre=centre, bounds=bounds, lock=lock, name=name,
            invertible=invertible, **metaparameters)

        # Set class-specific metaparameters
        # Nothing to do.

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATTRIBUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PRIVATE METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PUBLIC METHODS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

    @staticmethod
    def matrix2params(mat, **kwargs):
        """
        Creates quaternion from (3, 3) rotation matrix.

        :param mat: (3, 3) rotation matrix
        :type mat: np.ndarray
        :param kwargs:
            Keyword arguments required to retrieve quaternion from rotation
            matrix.
        :type kwargs: Any

        :returns: quaternion
        :rtype: np.ndarray

        """
        q = TxQuaternion._quaternion(mat[:3, :3])

        # If the rotation matrix is not about the origin, then the
        # centres must be identical.
        centre = kwargs.get("centre", False)
        if mat.shape[-1] == 4:
            rot = mat[:3, :3]
            trans = mat[:, -1]
            centre = np.linalg.inv(np.eye(3) - rot) @ trans.reshape(-1, 1)
            kwargs.update(centre=Point(centre.ravel()))
            assert np.allclose(mat @ centre, centre)
        if mat.shape[-1] == 3 and centre:
            raise AssertionError("Conflicting input: the rotation matrix is "
                                 "about the origin, but a centre point was "
                                 "also specified.")

        return np.ndarray(q, dtype=ts.DEFAULT_PARAMETER_DTYPE)

    @staticmethod
    def params2matrix(parameters, **kwargs):
        """
        Creates (3, 3) rotation matrix from quaternion.

        :param parameters: quaternion components
        :type parameters: Union[int, float]
        :param kwargs:
            Keyword arguments required to retrieve matrix from parameters.
        :type kwargs: Any

        :returns: (3, 3) rotation matrix
        :rtype: np.ndarray

        """
        qw, qx, qy, qz = parameters
        q_vect = np.asarray([qx, qy, qz]).reshape((-1, 1))
        Q = np.asarray([[0, -qz, qy],
                        [qz, 0, -qx],
                        [-qy, qx, 0]])
        I = np.eye(3)
        R = (qw ** 2 - np.dot(q_vect.T, q_vect)) * I \
            + 2 * np.dot(q_vect, q_vect.T) + 2 * qw * Q

        # Taking the rotation centre into account
        centre = kwargs.get("centre", False)
        if centre:
            mat = np.eye(4)
            mat[:3, :3] = R
            centre = Point(centre)
            trans = np.eye(4)
            trans[:-1, -1] = -centre.y
            R = np.linalg.inv(trans) @ mat @ trans
            R = R[:3, :]

        return R

    def inverse(self):
        """
        Creates an inverse transformation by negating the vector part of the
        quaternion.

        :returns: Transformation object that undoes the current rotation
        :rtype: TxRotationQuaternion

        """
        parameters = self.parameters.copy()
        qw, qx, qy, qz = parameters[:]
        meta = tu.rcopy(self.metaparameters)
        name = meta.pop("name")
        name = f"{name}_inv"
        shape = meta.pop("shape")
        centre = meta.pop("centre")
        invertible = meta.pop("invertible")
        # Use same invertibility, default bounds and no lock.
        obj = type(self)(qw, -qx, -qy, -qz, centre=centre, bounds=(-1, 1),
                         lock=None, name=name, invertible=invertible, **meta)
        return obj

    def quaternion(self):
        """ This function was overloaded to avoid unnecessary computations and
        the accumulation of numerical errors. """
        return tuple(self.parameters[:].tolist())

    def map(self, coords, signature=None):
        # Note: I left this here as a reminder that conjugating quaternions
        # with the coordinate vectors is equivalent to the matrix
        # computation. Hence, this map function could be overloaded if an
        # efficient quaternion-algebra is to be implemented in future versions.
        return super(TxQuaternion, self).map(coords, signature)

    def set(self, parameters, mask_or_indices=None):
        """
        This method sets the components of the quaternion while preserving its
        unit length. This is achieved by assigning the specified values for the
        specified components of the quaternion first and normalising the
        quaternion to unit length. This method then overrides the lock state of
        the ParameterVector, and assigns the newly computed parameter values to
        the transformation.

        :param parameters: new values for the specified quaternion components
        :type parameters: Union[tuple[float], list[float], np.ndarray]
        :param mask_or_indices: One of the following:
            1. 1-D Boolean mask that spans the entire parameter vector,
               including both locked and free parameters.
            2. A sequence of indices. Consecutive values of the delta array
               will be added to the parameters with the given indices.
            3. None (default). The lock state defined by the Transformation
               object's ParameterVector will apply when choosing the set of
               parameters that will be returned.
        :type mask_or_indices: Union[list, tuple, np.ndarray, None]

        """
        new_parameters = self.parameters[:]
        indices = self.parameters._mi(mask_or_indices, "indices")
        new_parameters.flat[indices] = \
            np.asarray(parameters, dtype=new_parameters.dtype)
        self.parameters[:] = new_parameters / np.linalg.norm(new_parameters)

    def update(self, delta, mask_or_indices=None):
        """
        This method updates the components of the quaternion while preserving
        its unit length. This is achieved by incrementing the specified
        components of the quaternion by the given amounts first and normalising
        the quaternion to unit length. This method then overrides the lock
        state of the ParameterVector, and assigns the newly computed parameter
        values to the transformation.

        :param delta: new values for the specified quaternion components
        :type delta: Union[tuple[float], list[float], np.ndarray]
        :param mask_or_indices: One of the following:
            1. 1-D Boolean mask that spans the entire parameter vector,
               including both locked and free parameters.
            2. A sequence of indices. Consecutive values of the delta array
               will be added to the parameters with the given indices.
            3. None (default). The lock state defined by the Transformation
               object's ParameterVector will apply when choosing the set of
               parameters that will be returned.
        :type mask_or_indices: Union[list, tuple, np.ndarray, None]

        """
        new_parameters = self.parameters[:]
        indices = self.parameters._mi(mask_or_indices, "indices")
        new_parameters.flat[indices] += \
            np.asarray(delta, dtype=new_parameters.dtype)
        self.parameters[:] = new_parameters / np.linalg.norm(new_parameters)

    # TODO: Deprecate Transformation.jacobian()
    # def jacobian(self, coords):
    #     coords = super(TxLinear, self).map(coords)
    #     qw, qx, qy, qz = self.parameters
    #     I = np.eye(3)
    #     Q = np.asarray([[0, -qz, qy],
    #                     [qz, 0, -qx],
    #                     [-qy, qx, 0]])
    #
    #     # Derivative of rotation matrix with respect to qw component
    #     dR_dqw = 2 * qw * I + 2 * Q
    #
    #     # Derivative of rotation matrix with respect to qx component
    #     dvvT_dqx = np.asarray([[2*qx, qy, qz],
    #                            [qy, 0, 0],
    #                            [qz, 0, 0]])
    #     dQ_dqx = np.asarray([[0, 0, 0],
    #                          [0, 0, -1],
    #                          [0, 1, 0]])
    #     dR_dqx = -2 * qx * I + 2 * dvvT_dqx + 2 * qw + dQ_dqx
    #
    #     # Derivative of rotation matrix with respect to qy component
    #     dvvT_dqy = np.asarray([[0, qx, 0],
    #                            [qx, 2*qy, qz],
    #                            [0, qz, 0]])
    #     dQ_dqy = np.asarray([[0, 0, 1],
    #                          [0, 0, 0],
    #                          [-1, 0, 0]])
    #     dR_dqy = -2 * qy * I + 2 * dvvT_dqy + 2 * qw + dQ_dqy
    #
    #     # Derivative of rotation matrix with respect to qz component
    #     dvvT_dqz = np.asarray([[0, 0, qx],
    #                            [0, 0, qy],
    #                            [qx, qy, 2*qz]])
    #     dQ_dqz = np.asarray([[0, -1, 0],
    #                          [1, 0, 0],
    #                          [0, 0, 0]])
    #     dR_dqz = -2 * qz * I + 2 * dvvT_dqz + 2 * qw + dQ_dqz
    #
    #     # (Homogenous) derivative tensor
    #     d = 3 + int(self.homogenise)
    #     dRh_dqw = np.zeros((d, d))
    #     dRh_dqw[:3, :3] = dR_dqw
    #     dRh_dqx = np.zeros((d, d))
    #     dRh_dqx[:3, :3] = dR_dqx
    #     dRh_dqy = np.zeros((d, d))
    #     dRh_dqy[:3, :3] = dR_dqy
    #     dRh_dqz = np.zeros((d, d))
    #     dRh_dqz[:3, :3] = dR_dqz
    #     t = np.stack((dRh_dqw, dRh_dqx, dRh_dqy, dRh_dqz), axis=0)
    #
    #     # Calculate Jacobian
    #     jac = np.einsum("ijk,hk", t, coords)
    #     return jac


if __name__ == "__main__":  # pragma: no cover
    print("""This module is not intended for execution.""")

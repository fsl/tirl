#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
import multiprocessing as mp
from itertools import product
from functools import partial


# TIRL IMPORTS
import tirl.utils as tu
import tirl.settings as ts
from tirl.constants import *
from tirl.dataobj import DataObject
from tirl.tirlobject import TIRLObject

# DEFINITIONS

process_wide_job_specific_globals = dict()


# DEVELOPMENT NOTES

"""
2021-May-31 INH
Be very careful when touching anything in this module. EvaluationManager is 
often called recursively, e.g. during the parallel evaluation of a TField 
through a chain of non-linear transformations, where the deformation field(s) 
also need(s) to be evaluated. Nested parallelisations are disabled by setting 
the "cpu" parameter of the EvaluationManager to 1 in daemon processes, hence 
all subsequent nested evaluations use a single process. However, the 
implementation relies on global variables, which are commonly used in parallel 
application to create shared data within a process. In those instances, the 
values of global variables would be replaced by consecutive evaluations. 
To prevent this, the global variables are instead stored in a global dict of 
dicts (process_wide_job_specific_globals), where evaluation calls are 
represented by unique tokens that are calculated from the time and the process 
ID. 

"""


# IMPLEMENTATION

class EvaluationManager(TIRLObject):

    def __init__(self, tf, **kwargs):
        super(EvaluationManager, self).__init__()
        self.tf = tf
        self.margin = kwargs.get("margin", 10)
        self.chunksize = kwargs.get("chunksize", ts.TFIELD_CHUNKSIZE)
        self.rule = kwargs.get("rule", ts.DEFAULT_TENSOR_XFM_RULE)

        # CPU: use the specified value or default to ts.CPU_CORES if the input
        # tensor field is known and the number of its elements exceeds the
        # preset chunking threshold. Otherwise fall back on single-core
        # processing. This is necessary because spawning new processes is a
        # very costly operation, making a single CPU faster for evaluations on
        # small domains.s
        # When pickled, the CPU number is set directly, but child processes
        # cannot multi-process to avoid an exponential shoot-out in the number
        # of TIRL processes.
        n_cpu = kwargs.get("n_cpu", None)
        if n_cpu is None:
            if hasattr(self, "tf"):
                if hasattr(self.tf, "domain"):
                    if self.tf.domain.numel > ts.CHUNK_THRESHOLD:
                        n_cpu = ts.CPU_CORES
                    else:
                        n_cpu = 1
                else:
                    n_cpu = 1
            else:
                n_cpu = 1
        self.cpu = tu.verify_n_cpu(n_cpu)

    @classmethod
    def _load(cls, dump):
        margin = dump.get("margin")
        cpu = dump.get("cpu")
        chunksize = dump.get("chunksize")
        rule = dump.get("rule")
        return cls(None, margin=margin, cpu=cpu, chunksize=chunksize, rule=rule)

    def _dump(self):
        objdump = super(EvaluationManager, self)._dump()
        objdump.update(
            margin=self.margin,
            cpu=self.cpu,
            chunksize=self.chunksize,
            rule=self.rule
        )
        return objdump

    def copy(self):
        return type(self)(
            self.tf, margin=self.margin, n_cpu=self.cpu,
            chunksize=self.chunksize, rule=self.rule)

    @property
    def cpu(self):
        return self._cpu

    @cpu.setter
    def cpu(self, n):
        # Note: the EvaluationManager might operate in a daemon process, where
        # it is unable to fork more processes.
        if mp.parent_process() is None:
            self._cpu = tu.verify_n_cpu(n)
        else:
            self._cpu = 1

    def interpolate(self, coords, layout=None):
        """
        Finds the tensor values of the linked TField-like instance at the
        specified voxel coordinates. This method employs chunking and parallel
        processing.

        :param coords: target voxel coordinates
        :type coords: np.ndarray
        :param layout:
            If None (default), the output shape depends on the tensor layout of
            the input TField. If TENSOR_MAJOR, the shape of the result is
            (*tensor_shape, n_voxels). If VOXEL_MAJOR, the shape of the result
            is (n_voxels, *tensor_shape).
        :type layout: None or str

        :returns: interpolated tensors
        :rtype: np.ndarray

        """
        coords = np.atleast_2d(coords)
        jobs = self.get_jobs(*coords.shape)
        inbuff, coords, outbuff = self.prepare(coords)
        ipol = self.tf.interpolator.copy()
        ipol.data = None
        token = tu.timehash() + "_" + str(mp.current_process().pid)
        initargs = (token, None, inbuff, outbuff, coords, self.tf.layout,
                    ipol, self.margin, None, None)
        # Use parallel processing if necessary
        if self.cpu.real > 1:
            ctx = mp.get_context(ts.MP_CONTEXT)
            workerfunc = partial(self.worker, token)
            with ctx.Pool(processes=int(self.cpu.real),
                          initializer=self.init_worker,
                          initargs=initargs) as pool:
                pool.map(workerfunc, jobs)
                pool.close()
                pool.join()
        elif self.cpu.imag > 1:
            import multiprocessing.dummy as mt
            workerfunc = partial(self.worker, token)
            with mt.Pool(processes=int(self.cpu.imag),
                         initializer=self.init_worker,
                         initargs=initargs) as pool:
                pool.map(workerfunc, jobs)
                pool.close()
                pool.join()
        else:
            self.init_worker(*initargs)
            for job in jobs:
                self.worker(token, job)

        # Return output buffer
        out = self.finalise(inbuff, coords, outbuff, layout)

        global process_wide_job_specific_globals
        if process_wide_job_specific_globals:
            del process_wide_job_specific_globals[token]

        return out

    def evaluate(self, domain, rule=RULE_SSR, layout=None):
        """
        Finds the tensor values of the linked TField-like instance at the
        physical coordinates of the specified domain, and transforms the
        tensors such that they are expressed in the voxel basis of the new
        domain.

        :param domain: evaluation target
        :type domain: Domain
        :param layout:
            If None (default), the output shape depends on the tensor layout of
            the input TField. If TENSOR_MAJOR, the shape of the result is
            (*tensor_shape, n_voxels). If VOXEL_MAJOR, the shape of the result
            is (n_voxels, *tensor_shape).
        :type layout: None or str
        :param rule:
            Tensor transformation rule. Tensors are interpolated but not
            transformed if rule=None.
        :type rule: str or None

        :returns: interpolated and transformed tensors
        :rtype: np.ndarray

        """
        # If the TField is a 2D or 3D vector field, apply vector txs:
        if (self.tf.tdim == 1) and (self.tf.tsize in (2, 3)) \
                and (rule is not None):
            jobs = self.get_jobs(domain.numel, domain.ndim)
            tx = "vector"

        # If the TField is a 2D or 3D second-order tensor, apply matrix txs:
        elif (self.tf.tdim == 2) and (self.tf.tsize in (4, 9)) \
                and (rule is not None):
            jobs = self.get_jobs(domain.numel, domain.ndim)
            tx = "tensor"

        # In every other case, interpolate without tensor transformations:
        else:
            jobs = self.get_jobs(domain.numel, domain.ndim)
            tx = None

        # Prepare input buffer for computations
        vc = self.tf.domain.map_physical_coordinates(
            domain.get_physical_coordinates())
        inbuff, coords, outbuff = self.prepare(vc)
        # Do not compute chain if no transformation is required
        if tx is not None:
            chain = self.tf.domain.all_tx() + domain.all_tx().inverse()
        else:
            chain = None
        ipol = self.tf.interpolator.copy()
        ipol.data = None
        token = tu.timehash() + "_" + str(mp.current_process().pid)
        initargs = (token, tx, inbuff, outbuff, coords, self.tf.layout, ipol,
                    self.margin, chain, rule)

        # Use parallel processing if necessary
        if self.cpu.real > 1:
            ctx = mp.get_context(ts.MP_CONTEXT)
            workerfunc = partial(self.worker, token)
            with ctx.Pool(processes=int(self.cpu.real),
                          initializer=self.init_worker,
                          initargs=initargs) as pool:
                pool.map(workerfunc, jobs)
                pool.close()
                pool.join()
        elif self.cpu.imag > 1:
            import multiprocessing.dummy as mt
            workerfunc = partial(self.worker, token)
            with mt.Pool(processes=int(self.cpu.imag),
                         initializer=self.init_worker,
                         initargs=initargs) as pool:
                pool.map(workerfunc, jobs)
                pool.close()
                pool.join()
        else:
            self.init_worker(*initargs)
            for job in jobs:
                self.worker(token, job)

        # Destroy temporary containers
        out = self.finalise(inbuff, coords, outbuff, layout)

        global process_wide_job_specific_globals
        if process_wide_job_specific_globals:
            del process_wide_job_specific_globals[token]

        return out

    def get_jobs(self, n_points, ndim):
        """
        Returns linear chunk indices for distributed interpolation/evaluation.

        :param n_points: number of input voxel coordinates
        :type n_points: int

        :returns: start and end indices of the chunks
        :rtype: tuple[int]

        """
        import psutil
        factor = self.tf.numel / n_points
        itemsize = (1 + factor) * self.tf.dtype.itemsize + \
                   ndim * np.dtype(ts.DEFAULT_FLOAT_TYPE).itemsize
        chunksize = tu.nbytes(self.chunksize) or \
                    min(tu.nbytes(ts.TFIELD_INSTANCE_MEMORY_LIMIT),
                        psutil.virtual_memory().available)
        if self.cpu.real > 1:
            n_cpu = int(self.cpu.real)
        elif self.cpu.imag > 1:
            n_cpu = int(self.cpu.imag)
        else:
            n_cpu = 1
        chunksize = max(int(chunksize / itemsize / n_cpu), 1)
        for start in range(0, n_points, chunksize):
            stop = int(min(start + chunksize, n_points))
            yield start, stop

    def prepare(self, c):
        # Set input buffer
        dataobj = self.tf._dataobj
        parallelisation = (self.cpu.real > 1) or (self.cpu.imag > 1)
        if dataobj.kind in ("Memory", "Buffer"):
            if parallelisation:
                inbuff = DataObject(
                    "shared", shape=self.tf.shape, dtype=self.tf.dtype)
                inbuff.value[...] = dataobj.value[...]
                inbuff.flush()
            else:
                inbuff = dataobj
        elif dataobj.kind == "MemoryMap":
            inbuff = dataobj
        elif dataobj.kind == "SharedMemory":
            inbuff = dataobj
        else:
            raise ValueError(f"Invalid kind of DataObject: {dataobj.kind}")

        # Create coordinate buffer
        if isinstance(c, np.memmap):
            coords = DataObject(c, shape=c.shape, dtype=c.dtype)
            coords.owner = False  # since it existed before!
        elif parallelisation:
            coords = DataObject("shared", shape=c.shape, dtype=c.dtype)
            coords.value[...] = c[...]
            coords.flush()
        else:
            coords = DataObject(c)

        # Create output buffer
        if self.tf.order == TENSOR_MAJOR:
            shape = (*self.tf.tshape, c.shape[0])
        elif self.tf.order == VOXEL_MAJOR:
            shape = (c.shape[0], *self.tf.tshape)
        else:
            assert False, "Internal error. Please report bug."
        if parallelisation:
            kind = "shared" if self.tf.storage in (MEM, SHMEM) else "memmap"
            outbuff = DataObject(kind, shape=shape, dtype=self.tf.dtype)
        else:
            outbuff = DataObject(shape=shape, dtype=self.tf.dtype)

        # Retract ownership rights for the time of parallel processing
        self.datain_own = inbuff.owner
        self.dataout_own = outbuff.owner
        self.coords_own = coords.owner
        inbuff.owner = False
        outbuff.owner = False
        coords.owner = False

        return inbuff, coords, outbuff

    def finalise(self, inbuff, coords, outbuff, layout):
        # Enforce custom tensor layout
        layout = layout or self.tf.layout.order
        if self.tf.layout.order != layout:
            if layout == TENSOR_MAJOR:
                axes = (*tuple(range(1, 1 + self.tf.tdim)), 0)
                outshape = (*self.tf.tshape, -1)
                outbuff.value = \
                    outbuff.value.transpose(*axes).reshape(*outshape)
                outbuff.shape = outshape
            elif layout == VOXEL_MAJOR:
                axes = (self.tf.tdim, *tuple(range(self.tf.tdim)))
                outshape = (-1, *self.tf.tshape)
                outbuff.value = \
                    outbuff.value.transpose(*axes).reshape(*outshape)
                outbuff.shape = outshape
            else:
                raise AssertionError("Internal error. Please report bug.")

        # Restore ownership
        inbuff.owner = self.datain_own
        coords.owner = self.coords_own
        outbuff.owner = self.dataout_own

        # Deactivate temporary buffers
        # coords.deactivate()
        # inbuff.deactivate()

        # Return in-memory DataObject if the source was also in-mem
        inmem = outbuff.nbytes <= tu.nbytes(ts.TFIELD_INSTANCE_MEMORY_LIMIT)
        if (self.tf._dataobj.kind == "Memory") and inmem:
            newdataobj = DataObject(shape=outbuff.shape, dtype=outbuff.dtype)
            newdataobj.value[...] = outbuff.value[...]
            outbuff = newdataobj

        # Create output
        return outbuff

    def init_worker(self, token, tx, datain, dataout, c, lo, ipol, m, ch, r):
        global process_wide_job_specific_globals
        process_wide_job_specific_globals[token] = dict(
            input_buffer=datain,
            coordinates=c,
            output_buffer=dataout,
            interpolator=ipol,
            tfield_layout=lo,
            margin=m,
            transformation=tx,
            chain=ch,
            rule=r
            #lock=l
        )

    def worker(self, token, job):
        """
        Performs the interpolation/evaluation on a chunk of the input TField
        data.

        :param token: unique instance identifier (for embedded operations)
        :type token: str
        :param job: (start, end) linear indices to chunk of the input buffer
        :type job: tuple[int]

        """
        varargs = process_wide_job_specific_globals[token]
        input_buffer   = varargs["input_buffer"]
        coordinates    = varargs["coordinates"]
        output_buffer  = varargs["output_buffer"]
        interpolator   = varargs["interpolator"]
        tfield_layout  = varargs["tfield_layout"]
        margin         = varargs["margin"]
        transformation = varargs["transformation"]
        chain          = varargs["chain"]
        rule           = varargs["rule"]
        # lock           = varargs["lock"]

        start, stop = job
        pid = mp.current_process().pid
        vc = coordinates.value[start:stop]

        # Get bounding region
        if not interpolator.hold:
            low = np.maximum(
                np.min(vc, axis=0).astype(int) - margin, 0)
            high = np.minimum(
                np.max(vc, axis=0).astype(int) + margin, tfield_layout.vshape)
            low = np.minimum(low, np.subtract(tfield_layout.vshape, 1))
            high = np.maximum(high, low + 1)
            internal_coords = vc - low
        else:
            internal_coords = vc

        # Interpolate tensor components separately
        t_index = product(*tuple(range(dim) for dim in tfield_layout.tshape))
        for tix in t_index:
            if tfield_layout.order == TENSOR_MAJOR:
                if not interpolator.hold:
                    dslicer = \
                        tix + tuple(slice(lo, hi) for lo, hi in zip(low, high))
                else:
                    dslicer = (*tix, Ellipsis)
                oslicer = (*tix, slice(start, stop))
            else:
                if not interpolator.hold:
                    dslicer = \
                        tuple(slice(lo, hi) for lo, hi in zip(low, high)) + tix
                else:
                    dslicer = (Ellipsis, *tix)
                oslicer = (slice(start, stop), *tix)
            interpolator.data = input_buffer[dslicer]
            res = interpolator(internal_coords).ravel()
            # lock.acquire()
            # output_buffer.reload()
            output_buffer[oslicer] = res
            output_buffer.flush()
            # lock.release()

        # Transform tensors if needed

        if tfield_layout.order == TENSOR_MAJOR:
            slicer = (Ellipsis, slice(start, stop))
            if (transformation is None) or (chain is None) or (rule is None):
                pass
            elif transformation == "vector":
                vects = output_buffer[slicer].reshape(-1, stop - start).T
                res = chain.map_vector(
                    vects=vects, coords=vc, rule=rule).T.flat[...]
                # lock.acquire()
                output_buffer[slicer].flat[...] = res
                output_buffer.flush()
                # lock.release()
            elif transformation == "tensor":
                tensors = output_buffer[slicer].moveaxis(-1, 0)
                t = chain.map_tensor(tensors=tensors, coords=vc, rule=rule)
                res = t.moveaxis(0, -1).flat[...]
                # lock.acquire()
                output_buffer[slicer].flat[...] = res
                output_buffer.flush()
                # lock.release()
            else:
                raise ValueError(f"Invalid tensor transformation "
                                 f"instruction: {transformation}")
        elif tfield_layout.order == VOXEL_MAJOR:
            slicer = (slice(start, stop), Ellipsis)
            if (transformation is None) or (chain is None) or (rule is None):
                pass
            elif transformation == "vector":
                vects = output_buffer[slicer].reshape(stop - start, -1)
                res = chain.map_vector(
                    vects=vects, coords=vc, rule=rule).flat[...]
                # lock.acquire()
                output_buffer[slicer].flat[...] = res
                output_buffer.flush()
                # lock.release()
            elif transformation == "tensor":
                tensors = output_buffer[slicer]
                t = chain.map_tensor(tensors=tensors, coords=vc, rule=rule)
                res = t.flat[...]
                # lock.acquire()
                output_buffer[slicer].flat[...] = res
                output_buffer.flush()
                # lock.release()
            else:
                raise ValueError(f"Invalid tensor transformation "
                                 f"instruction: {transformation}")
        else:
            raise ValueError(f"Invalid tensor layout: {tfield_layout.order}")

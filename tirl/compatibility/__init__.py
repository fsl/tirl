#!/usr/bin/env python

import tirl


def update_dump_version(dump, version):
    """
    Creates an up-to-date TIRLObject dump from an existing object dump that
    was created by a previous version of TIRL.

    """
    if (version[0] < 2):
        from .tirl1 import update_dump
        return update_dump(dump)
    if (version[0] == 2) and (version[1] >= 0):
        from .tirl2 import update_dump
        return update_dump(dump)
    else:
        return dump


def newdump(*args, **kwargs):
    """
    New object dump creation method that ensures that the "id" and "type"
    parameters are filled, as these are mandatory target identification labels
    in TIRL. The parameters are taken from the previous dump, but no other
    parameters are carried over beyond these two.

    """
    if args:
        dump = args[0]
    else:
        dump = {}
    return dict(
        id=dump.get("id", kwargs.pop("id", None)),
        type=dump.get("type", kwargs.pop("type", None)),
        **kwargs
    )

#!/usr/bin/env python

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT

"""
Compatibility layer for loading TIRL 1.x files.

"""

# DEPENDENCIES

import numpy as np
import tirl.settings as ts
from tirl.compatibility import newdump


# DEVELOPMENT NOTES

"""
Compatibility layer for legacy internal versions.

"""


# IMPLEMENTATION

def update_dump(dump):
    """
    Creates an up-to-date TIRLObject dump from an existing object dump that
    was created by TIRL<2.0.0. This function processes the dump in full depth.
    It is assumed that TIRLObject dumps exist in dicts and lists only.

    """
    # New module names
    module_update_rules = {
        "tirl.transformations.linear"   : "tirl.transformations",
        "tirl.transformations.nonlinear": "tirl.transformations",
        "tirl.transformations.basic"    : "tirl.transformations",
        "tirl.costs"                    : "tirl.cost",
        "tirl.interpolators"            : "tirl.interpolation",
        "tirl.regularisers"             : "tirl.regularisation"
    }

    if isinstance(dump, dict):
        for key, value in dump.items():
            dump[key] = update_dump(value)
        objtype = dump.get("type", None)
        if objtype is not None:
            for key, value in module_update_rules.items():
                objtype = objtype.replace(key, value)
            else:
                dump.update(type=objtype)
            func = str(objtype).split(".")[-1].lower()
            return globals().get("ud_" + func, lambda x: x)(dump)
        else:
            return dump
    elif isinstance(dump, list):
        for i, item in enumerate(dump):
            dump[i] = update_dump(item)
        return dump
    else:
        return dump


def ud_domain(dump):
    """
    Update dump: Domain

    """
    from tirl.utils import rndstr
    extent = dump.get("extent")
    if isinstance(extent, np.ndarray):
        coordinates = extent
        shape = (coordinates.shape[0],)
    else:
        coordinates = None
        shape = extent

    # Eliminate TxDirect from non-compact domains
    internal_chain = dump.get("offset")
    if internal_chain:
        if (internal_chain[0]["type"].split(".")[-1] == "TxDirect"):
            internal_chain.pop(0)
        internal_chain = dict(
            type="tirl.chain.Chain",
            id=rndstr(16),
            transformations=internal_chain
        )
    external_chain = dump.get("chain")
    if external_chain:
        external_chain = dict(
            type="tirl.chain.Chain",
            id=rndstr(16),
            transformations=external_chain
        )
    newdomaindump = newdump(dump,
        name=dump.get("name"),
        shape=shape,
        coordinates=coordinates,
        dtype=None,
        internal=internal_chain,
        external=external_chain,
        memlimit=dump.get("instance_mem_limit"),
        storage=dump.get("storage")
    )
    # Eliminate TxDirect from non-compact domains
    offset = newdomaindump["internal"]
    if offset and offset[0]["type"].split(".")[-1] == "TxDirect":
        newdomaindump["internal"].pop(0)
    return newdomaindump


def ud_tfield(dump):
    """
    Update dump: TField

    """
    import dill
    from tirl.layout import Layout
    from tirl.domain import Domain
    from tirl.evmgr import EvaluationManager
    props = dump.get("properties")
    domain = Domain.hload(props.get("extent"))
    try:
        header = dill.dumps(props.get("kwargs"))
    except:
        header = {}

    return newdump(dump,
        data=dump.get("data"),
        layout=Layout(vshape=domain.shape,
                      tshape=props.get("tensor_shape"),
                      order=props.get("order")),
        domain=domain,
        interpolator=props.get("interpolator"),
        header=header,
        name=props.get("name"),
        resolution=None,
        evmgr=EvaluationManager(None),
        rule=None  # before v3.0 tensors were not transformed by default
    )


def ud_timage(dump):
    """
    Update Dump: TImage

    """
    from tirl.resmgr import ResolutionManager
    from tirl.domain import Domain
    from tirl.tfield import TField

    data = dump.get("data")
    header = dump.get("header")
    mask = dump.get("mask")
    props = dump.get("properties")
    domain = Domain.hload(props.get("extent"))
    ipol = props.get("interpolator")
    name = props.get("name")
    order = props.get("order")
    tshape = props.get("tensor_shape")

    im = TField(data, domain=domain, tshape=tshape, order=order,
                name=name, interpolator=ipol)
    resmgr = ResolutionManager(im)
    if mask is not None:
        maskmgr = ResolutionManager(mask)
    else:
        maskmgr = None

    return newdump(dump,
        resmgr=resmgr,
        maskmgr=maskmgr,
        header=header,
        name=name
    )


def ud_resolutionmanager(dump):
    """
    Update dump: ResolutionManager

    """
    # RMv2 and RMv3 are completely different objects, only the name was kept.
    # The RMv2 dump is essentially a v2 TImage dump.
    # Use the ud_timage function to retrieve the contents if necessary.
    # This function returns the dump, so that it can be ignored by ud_timage.
    return dump


def ud_rbfinterpolator(dump):
    """
    Update dump: RbfInterpolator

    """
    from tirl.interpolation.rbfinterpolator import RbfInterpolator

    defaults = RbfInterpolator.default_options.copy()
    data = dump.get("values")
    kwargs = dump.get("kwargs")
    basis = kwargs.pop("basis")
    defaults.update(kwargs)
    kwargs = {k: v for k, v in defaults.items() if k in
              ("epsilon", "function", "mode", "norm", "smooth")}
    kwargs.update(basis=basis)

    return newdump(dump,
        data=data,
        kwargs=kwargs
    )

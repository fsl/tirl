#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np


# TIRL IMPORTS

from tirl.cost.cost import Cost


# DEFINITIONS


# IMPLEMENTATION

class CostPoints(Cost):
    """
    CostPoints - Point-Based Correspondence cost function term

    """
    def __init__(self, source, target, vx_src, vx_trg, logger=None,
                 normalise=True, **metaparameters):
        """
        Initialisation of CostMSD.

        :param source:
            Moving image. Values of the source image are interpolated at the
            points of the target domain.
        :type source: TImage
        :param target:
            Fixed image. Values of the source image are interpolated at the
            points of the target domain.
        :type target: TImage
        :param metaparameters:
            Additional keyword arguments.
        :type metaparameters: Any

        """
        # Call superclass initialisation
        super(CostPoints, self).__init__(
            source, target, logger=logger, **metaparameters
        )
        self.source_points = np.atleast_2d(vx_src)
        self.target_points = np.atleast_2d(vx_trg)

        # Set class-specific metaparameters
        if isinstance(normalise, bool):
            self.metaparameters.update(normalise=normalise)
        else:
            raise TypeError(f"Expected bool value for normalise option, "
                            f"got {normalise.__class__.__name__}")

    def function(self, target, source, combined_mask):
        """
        Calculates the scalar cost based on squared differences between the
        target and the source constituents. If the normalise option is set
        (default), the cost is normalised by the number of foreground voxels.

        """
        srcp = self._source.domain.map_voxel_coordinates(self.source_points)
        trgp = self._target.domain.map_voxel_coordinates(self.target_points)

        costs = np.sum(np.subtract(srcp, trgp) ** 2, axis=-1)

        if self.metaparameters.get("normalise"):
            return np.sum(costs) / len(costs)
        else:
            return np.sum(costs)

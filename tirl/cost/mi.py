#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEVELOPMENT NOTES

# Part of the code in this module was adopted from:
# http://pythonhosted.org/MedPy/_modules/medpy/metric/image.html

# The copyright information of the original source file:
#
# <COPYRIGHT>
#
# Copyright (C) 2013 Oskar Maier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# author Oskar Maier
# version r0.1.0
# since 2013-07-09
# status Release
#
# </COPYRIGHT>


# DEPENDENCIES

import numpy as np
from numbers import Integral
from scipy.ndimage.interpolation import map_coordinates


# TIRL IMPORTS

from tirl.cost.cost import Cost


# DEFINITIONS


# IMPLEMENTATION

class CostMI(Cost):
    """
    Mutual information and normalised mutual information of two images. Note
    that the class returns MI with a negative sign to conform with optimisers
    that try to minimise the objective function.

    """

    RESERVED_KWARGS = ("source", "target", "target_filter", "source_prefilter",
                       "source_postfilter", "maskmode", "matchmode", "logger",
                       "metaparameters")

    def __init__(self, source, target, target_filter=None,
                 source_prefilter=None, source_postfilter=None,
                 maskmode="and", matchmode="l2_norm", normalise=False, bins=32,
                 logger=None, **metaparameters):
        """
        Initialisation of CostMI.

        """
        # Call parent-class initialisation
        super(CostMI, self).__init__(
            source, target, target_filter=target_filter,
            source_prefilter=source_prefilter,
            source_postfilter=source_postfilter, maskmode=maskmode,
            matchmode=matchmode, logger=logger, **metaparameters)

        # Set class specific metaparameters
        self.normalise = normalise
        self.bins = bins

    @property
    def normalise(self):
        return self.metaparameters.get("normalise")

    @normalise.setter
    def normalise(self, n):
        if isinstance(n, (bool, np.bool_)):
            self.metaparameters.update(normalise=bool(n))
        else:
            raise TypeError(f"Expected boolean for 'normalise' option, "
                            f"got {n} instead.")

    @property
    def bins(self):
        return self.metaparameters.get("bins")

    @bins.setter
    def bins(self, n):
        if isinstance(n, Integral) and (n > 0):
            self.metaparameters.update(bins=n)
        else:
            raise TypeError(f"Expected a positive integer for the number of "
                            f"bins, got {n} instead.")

    @staticmethod
    def _range(a, bins):
        """
        Computes the histogram range of the values in the array 'a' according
        to scipy.stats.histogram.

        """
        a = np.asarray(a)
        a_max = np.nanmax(a)
        a_min = np.nanmin(a)
        s = 0.5 * (a_max - a_min) / float(bins - 1)
        return a_min - s, a_max + s

    def _get_distributions(self, source, target, bins, mask):
        """

        """
        # Given that this method is called after the resampling step, this
        # should never be violated. Just including it here for safety.
        if source.vshape != target.vshape:
            raise AssertionError("The source and target images must "
                                 "have identical shapes.")

        # This class requires scalar images (after the tensor matching has been
        # applied.)
        if source.tdim or target.tdim:
            raise AssertionError(f"{self.__class__.__name__} requires the "
                                 f"source and target images be scalar-valued "
                                 f"after tensor matching. Please use an "
                                 f"appropriate tensor matching scheme.")

        # Compute input data ranges
        if mask is None:
            sdata = source.data
            tdata = target.data
        else:
            sdata = source.voxels[mask]
            tdata = target.voxels[mask]

        source_range = self._range(sdata, bins)
        target_range = self._range(tdata, bins)

        # Compute joint distribution and marginals
        joint_hist = np.histogram2d(sdata.ravel(), tdata.ravel(),
            bins=bins, range=[source_range, target_range])[0]
        marginal_source = np.histogram(sdata, bins=bins, range=source_range)[0]
        marginal_target = np.histogram(tdata, bins=bins, range=target_range)[0]

        return joint_hist, marginal_source, marginal_target

    @staticmethod
    def _entropy(data):
        """
        Computes the entropy of a dataset. Ignores zeros.

        """
        data = data / np.sum(data)
        data = data[np.nonzero(data)]
        return -np.sum(data * np.log2(data))

    def __call__(self, *args, **kwargs):
        """
        Calculates the scalar cost by calling the class-specific cost function.

        :return: scalar cost function term (a measure of image dissimilarity)
        :rtype: float

        """
        # Note: the reason for overriding the parent-class __call__ method is
        # to force tensor matching. In the parent class implementation, if the
        # tensor shapes are conformant, the matching is skipped. For this
        # class, it is good to run it by default, as the mutual information is
        # calculated for gray values.

        # Get target and source constituents
        target, source = self.get_constituents(force_match=True)

        # Combine masks
        # TODO: Think about how this could be calculated more sparingly.
        combined_mask = self.combine_masks(target.mask, source.mask)
        if combined_mask is not None:
            if np.allclose(combined_mask, 0):
                raise AssertionError("All elements of the mask are zero.")

        # Calculate scalar cost
        return float(self.function(target, source, combined_mask))

    def function(self, target, source, combined_mask):
        """
        Calculates the scalar (normalised or unnormalised) mutual information
        metric. Note that the class returns the negative mutual information to
        conform with optimisers that perform minimisation by default.

        """
        # If a combined mask exists, binarise it and use it to discard certain
        # points from the distributions.
        if combined_mask is not None:
            amax = np.max(combined_mask)
            if not np.isclose(amax, 0):
                combined_mask = (combined_mask / amax) > 0.5

        d_joint, d_source, d_target = self._get_distributions(
            source, target, bins=self.bins, mask=combined_mask)

        # Compute joint entropy and separate entropies
        h_joint = CostMI._entropy(d_joint)
        h_source = CostMI._entropy(d_source)
        h_target = CostMI._entropy(d_target)

        # Compute and return the mutual information distance
        if self.normalise:
            return -(h_source + h_target - h_joint) \
                    / float(h_source + h_target)
        else:
            return -float(h_source + h_target - h_joint)

    def dx(self, dim=None):
        """
        Returns the cost Jacobian, i.e. the voxelwise total derivative of the
        negative (normalised or unnormalised) mutual information metric with
        respect to the local displacement vector. The result has the following
        shape: (voxels, gradient_directions).

        Note that this quantity is calculated on the voxel grid of the target
        image. As a consequence, to obtain a valid parameter Jacobian of
        the total cost, the result of this function must be multiplied by
        transformation Jacobians that have been mapped onto the voxel space of
        the target image.

        The implementation is based on (Hermosillo et al, 2002) and
        (Avants et al, 2011).

        """
        source, target = self.get_constituents(force_match=True)

        # If there is a combined mask, binarise it and use it to discard
        # certain points from the distributions.
        combined_mask = self.combine_masks(target.mask, source.mask)
        if combined_mask is not None:
            amax = np.max(combined_mask)
            if not np.isclose(amax, 0):
                combined_mask = (combined_mask / amax) > 0.5

        d_joint, d_source, d_target = self._get_distributions(
            source, target, bins=self.bins, mask=combined_mask)
        dq_source = np.gradient(d_joint, axis=0)

        amin, amax = self._range(source.data, self.bins)
        p = (source.data.ravel() - amin) / amax
        amin, amax = self._range(target.data, self.bins)
        q = (target.data.ravel() - amin) / amax

        b = self.bins - 1
        points = np.stack((p * b, q * b), axis=0)
        points = np.clip(points, 0, b)
        Q = map_coordinates(d_joint, points, order=1)
        dQ = map_coordinates(dq_source, points, order=1)

        # f = map_coordinates(d_source, p * b)
        from scipy.interpolate import interp1d
        d_source = np.sum(d_joint, 1)  # TODO: Add Parzen window!
        dd_source = np.gradient(d_source)
        f = interp1d(np.linspace(0, 1, self.bins), d_source, kind="linear")(p)
        df = interp1d(np.linspace(0, 1, self.bins), dd_source, kind="linear")(p)
        factor = dQ / Q - df / f
        factor[~np.isfinite(factor)] = 0
        if np.any(~np.isfinite(factor)):
            raise

        if dim is None:
            dim = source.vaxes
            grad = np.stack(np.gradient(source, axis=dim), axis=-1)
            jac = factor[..., np.newaxis] * grad.reshape(-1, len(dim))

        elif isinstance(dim, int):
            dim = source.vaxes[dim]
            grad = np.gradient(source, axis=dim)
            jac = factor * grad.reshape(-1)

        else:
            raise TypeError("Invalid dimension specification.")

        # Masked point should contribute no gradient force for the registration
        if combined_mask is not None:
            jac[np.isclose(combined_mask, 0, atol=1e-3).ravel(), ...] = 0

        # return -jac
        return jac

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
import nibabel as nib

# DEFINITIONS

from tirl.constants import *

FSL_FNIRT_DISPLACEMENT_FIELD       = 2006
FSL_CUBIC_SPLINE_COEFFICIENTS      = 2007
FSL_DCT_COEFFICIENTS               = 2008
FSL_QUADRATIC_SPLINE_COEFFICIENTS  = 2009


# IMPLEMENTATION

def load_mat(matfile, input=None, reference=None):
    """
    Converts a FLIRT matrix to a TIRL chain. The chain maps the voxel
    coordinates of the input to the voxel coordinates of the reference, hence
    when it is part of TIRL chain, it must be embedded in a suitable
    transformation bracket that maps between physical and voxel coordinates:
    phys2vox(input) > (FLIRT chain) > vox2phys(reference).

    Explanation of the FLIRT-equivalent chain:
    FLIRT matrices represent affine transformations between the mm-coordinates
    of two images that are both represented in RADIOLOGICAL orientation. As
    NIfTI images can be either in RADIOLOGICAL or NEUROLOGICAL orientation,
    this conversion is carried out implicitly in FSL tools, hence the
    transformation represented by the stand-alone .mat file is ambiguous to
    these two parameters. If the input and output NIfTI volumes are not
    specified, it is assumed that both the input and the reference are in
    RADIOLOGICAL orientation. (WARNING: one must check whether this assumption
    is true, otherwise the mapping may result in left-right side reversal!)

    :param matfile: FLIRT matrix file (with extension .mat)
    :type matfile: str

    :returns: TIRL transformation chain equivalent to the FLIRT transformation
    :rtype: Chain

    """
    from tirl.chain import Chain
    from tirl.transformations.affine import TxAffine

    mat = np.loadtxt(matfile)
    swapmat = np.diag([-1., 1., 1., 1.])

    if input is not None:
        hdr = nib.load(input).header
        sform = hdr.get_best_affine()
        xdim, ydim, zdim, *dims = hdr.get_zooms()
        # Define the sampling matrix for the input image
        sign = np.sign(np.linalg.det(sform))
        ism = np.diag([xdim, ydim, zdim, 1.0])
        if sign > 0:
            xsize = hdr.get_data_shape()[0]
            swapmat[0, -1] = xsize - 1
            ism = ism @ swapmat
    else:
        ism = np.eye(4)

    if reference is not None:
        hdr = nib.load(reference).header
        sform = hdr.get_best_affine()
        xdim, ydim, zdim, *dims = hdr.get_zooms()
        # Define the sampling matrix for the reference image
        sign = np.sign(np.linalg.det(sform))
        rsm = np.diag([xdim, ydim, zdim, 1.0])
        if sign > 0:
            xsize = hdr.get_data_shape()[0]
            swapmat[0, -1] = xsize - 1
            rsm = rsm @ swapmat
    else:
        rsm = np.eye(4)

    # Create equivalent TIRL chain (maps voxel to voxel coordinates!)
    # Lock the parameters of the bracket transformations, so that they are
    # automatically ignored by Optimisers.
    irsm = TxAffine(np.linalg.inv(rsm), name="frsm", lock="all")
    flirtmat = TxAffine(mat, name="flirtmat")
    ism = TxAffine(ism, name="fism", lock="all")

    return Chain(ism, flirtmat, irsm)


def load_warp(warpfile, moving=None):
    """
    Converts a FNIRT-generated warp field into an equivalent TIRL
    transformation chain.

    Explanation:
    A FNIRT field represents a non-linear transformation that warps image A
    onto image B. However, the forward transformation A->B is represented using
    pull-backs, which are defined in the opposite sense. The voxels of the
    FNIRT field and the voxels of B are in one-to-one correspondence. Each
    voxel in the field contains 3 values that represent the displacement vector
    from the mm-coordinates of that voxel and the mm coordinates of its image
    in A. The displacement vectors of the field can therefore be used to
    project out from the voxels of B to define sampling locations in A.
    Sampling of A at the projected points is then realised by interpolation
    to create a warped version of A on the domain of B.

    FSL further defines the mm coordinates of the images in RADIOLOGICAL
    orientation. As the moving image may be either in RADIOLOGICAL or
    NEUROLOGICAL orientation, at least the moving image must be provided to
    eliminate this ambiguity of the FNIRT transformation. If the moving image
    is not specified, it is assumed that it has RADIOLOGICAL orientation and
    has identical sampling (resolution) as the warp field.

    Example problem: Consider a situation where image A has an existing TIRL
    chain that maps coordinates x_A to the domain of image B (x_B), and there
    is an independent third image, image C for which we have a FNIRT
    transformation that warps B to C. The goal is to obtain an invertible and
    interpolation-free mapping of x_A to x_C.

    Solution: invert the FNIRT transformation using invwarp (FSL), and append
    it to end of the existing chain. When importing the inverse transformation
    to TIRL, use image C as the moving image reference.

    Note: as of TIRL v1.1, this method can only load dense displacement fields.
    Coefficient fields generated by FNIRT must be converted to field format
    using FSL's 'fnirtfileutils' command before they are imported to TIRL.

    """
    import nibabel as nib
    from tirl.transformations.affine import TxAffine

    # Load data from Nifti
    nifti = nib.load(warpfile)
    hdr = nifti.header
    intent = hdr.get_intent("code")[0]

    if intent != FSL_FNIRT_DISPLACEMENT_FIELD:
        import warnings
        warnings.warn("The nifti intent code of the warp field is different "
                      "from what is expected. Make sure that the input is a "
                      "dense displacement field that was generated by FNIRT.")
        # raise NotImplementedError(
        #     "Unsupported FNIRT warp format. Please convert the warp to field "
        #     "format using fnirtfileutils in FSL.")
    chain = _load_fnirt_field(nifti)

    if moving is not None:
        hdr = nib.load(moving).header
        sform = hdr.get_best_affine()
        xdim, ydim, zdim, *dims = hdr.get_zooms()
        # Define the sampling matrix for the moving image
        sign = np.sign(np.linalg.det(sform))
        msm = np.diag([xdim, ydim, zdim, 1.0])
        if sign > 0:
            xsize = hdr.get_data_shape()[0]
            swapmat = np.diag([-1., 1., 1., 1.])
            swapmat[0, -1] = xsize - 1
            msm = msm @ swapmat
    else:
        msm = np.eye(4)

    chain = chain + TxAffine(np.linalg.inv(msm), name="imsm", lock="all")

    return chain


def _load_fnirt_field(nifti):
    """
    Loads a FNIRT-generated dense displacement field.

    :param nifti: 4D FNIRT field file (_field.nii.gz)
    :type nifti: nib.Nifti1Header

    :returns: TxDisplacementField object with loaded deformations
    :rtype: TxDisplacementField

    """
    import numpy as np
    from tirl.chain import Chain
    from tirl.domain import Domain
    from tirl.tfield import TField
    from tirl.transformations.affine import TxAffine
    from tirl.interpolation.scipyinterpolator import ScipyInterpolator
    from tirl.transformations.displacement import TxDisplacementField

    # Load metadata from NiftiHeader
    hdr = nifti.header
    sform = hdr.get_best_affine()
    xdim, ydim, zdim, *dims = hdr.get_zooms()
    xsize, ysize, zsize, *sizes = hdr.get_data_shape()
    sign = np.sign(np.linalg.det(sform))
    sampling_mat = np.diag([xdim, ydim, zdim, 1.0])
    if sign > 0:
        swapmat = np.diag([-1., 1., 1., 1.])
        swapmat[0, -1] = xsize - 1
        sampling_mat = sampling_mat @ swapmat

    # Create the voxel domain of the warp
    sm = TxAffine(sampling_mat, name="wsm", lock="all")
    warp_voxel_domain = Domain(xsize, ysize, zsize, transformations=sm)

    # Load displacements from Nifti dataobj
    field = TField(
        np.asanyarray(nifti.dataobj), taxes=(3,), domain=warp_voxel_domain,
        copy=False, order=VOXEL_MAJOR, interpolator=ScipyInterpolator,
        storage=MEM)
    field.order = TENSOR_MAJOR

    # Create TxDisplacementField with affine pre-transform
    tx = TxDisplacementField(
        field, invertible=True, mode=NL_ABS, name="fnirtwarp")

    # Create chain that maps voxel
    return Chain(sm, tx)


def _estimate_affine_component(field, sampling_mat):
    """
    Estimates the affine component of a dense displacement field. Implements an
    adaptation of the equivalent method defined in
    FSL/warpfns/fnirt_file_reader.

    """
    import numpy as np
    xsize, ysize, zsize = field.shape[:3]
    field = field.reshape(-1, 3)
    vc = np.stack((*np.mgrid[0:xsize, 0:ysize, 0:zsize],
                  np.ones((xsize, ysize, zsize))), axis=-1).reshape(-1, 4)
    X = np.einsum("ij,...j", sampling_mat, vc)
    affine = np.eye(4, dtype=X.dtype)
    for row in range(3):
        yp = np.einsum("i,...i", sampling_mat[row], vc) + field[:, row]
        affine[row] = np.einsum("ij,j", np.linalg.pinv(X), yp)
    else:
        return np.linalg.inv(affine)


def _toggle_affine_component(field, sampling_mat, affine, op):
    """
    Removes the affine component from a dense displacement field. The operation
    is in-place. Implements an adaptation of the equivalent method defined in
    FSL/warpfns/fnirt_file_reader.

    :param op: "add" or "subtract"
    :type op: str

    """
    from operator import add, sub
    op = {"add": add, "subtract": sub}.get(op)

    mat = (np.linalg.inv(affine) - np.eye(4)) @ sampling_mat
    xsize, ysize, zsize = field.shape[:3]
    field = field.reshape(-1, 3)
    vc = np.stack((*np.mgrid[0:xsize, 0:ysize, 0:zsize],
                   np.ones((xsize, ysize, zsize))), axis=-1).reshape(-1, 4)
    for row in range(3):
        field[:, row] = op(field[:, row], np.einsum("i,...i", mat[row], vc))


def load_dti(basename):

    import numpy as np
    from glob import glob
    from tirl.tfield import TField
    from tirl.timage import TImage
    from tirl.operations.tensor import TensorOperator
    from tirl.transformations.affine import TxAffine

    # Load eigenvectors and eigenvalues
    l1 = TImage(glob(basename + "_L1.nii*")[0], order=VOXEL_MAJOR)
    l2 = TImage(glob(basename + "_L2.nii*")[0], order=VOXEL_MAJOR)
    l3 = TImage(glob(basename + "_L3.nii*")[0], order=VOXEL_MAJOR)
    v1 = TImage(glob(basename + "_V1.nii*")[0], order=VOXEL_MAJOR)
    v2 = TImage(glob(basename + "_V2.nii*")[0], order=VOXEL_MAJOR)
    v3 = TImage(glob(basename + "_V3.nii*")[0], order=VOXEL_MAJOR)

    eigenform = TField(vshape=l1.vshape, tshape=(3, 3), order=VOXEL_MAJOR)
    eigenbasis = TField(vshape=v1.vshape, tshape=(3, 3), order=VOXEL_MAJOR)
    for i, (l, v) in enumerate(zip([l3, l2, l1], [v3, v2, v1])):
        eigenform.tensors[i, i] = l
        eigenbasis.tensors[:, i] = v

    mask = l1 > 0
    invert = TensorOperator(np.linalg.inv)
    invert.mask = mask
    eigenbasis_inv = TField.zeros_like(eigenbasis)
    invert(eigenbasis, out=eigenbasis_inv)
    diffusion_tensors = TImage(eigenbasis @ eigenform @ eigenbasis_inv)

    # Attach NIfTI header, and add sform matrix to the transformation chain
    diffusion_tensors.header = l1.header
    sform = diffusion_tensors.header["nifti_header"].get_best_affine()
    diffusion_tensors.domain.chain.append(TxAffine(sform, name="sform"))

    return diffusion_tensors


class FlirtMatrix(object):
    """
    FlirtMatrix is a Transformation-like and Chain-like adaptor that helps with
    the importing of FLIRT matrix files into a TIRL work environment.

    """
    def __init__(self, matfile, input=None, reference=None):
        self.matfile = matfile
        self.input = input
        self.reference = reference

    def __call__(self, *args, **kwargs):
        chain = load_mat(
            self.matfile, input=self.input, reference=self.reference)
        return chain

    def __add__(self, other):
        return self() + other   # reduces to Chain addition

    def __radd__(self, other):
        return other + self()   # reduces to Chain addition

    def __repr__(self):
        return f"FlirtMatrix(in={self.input}, reference={self.reference})"

    def copy(self):
        return FlirtMatrix(
            matfile=self.matfile, input=self.input, reference=self.reference)

    def inverse(self):
        return self().inverse()

    def matrix(self):
        return self().reduce().matrix


if __name__ == "__main__":  # pragma: no cover
    print("This module is not indended for execution.")

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _       __      __  _         _
#  |__   __| |_   _| |  __ \  | |      \ \    / / (_)       (_)
#     | |      | |   | |__) | | |       \ \  / /   _   ___   _    ___    _ __
#     | |      | |   |  _  /  | |        \ \/ /   | | / __| | |  / _ \  | '_ \
#     | |     _| |_  | | \ \  | |____     \  /    | | \__ \ | | | (_) | | | | |
#     |_|    |_____| |_|  \_\ |______|     \/     |_| |___/ |_|  \___/  |_| |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from functools import reduce
from operator import mul
from skimage.exposure import rescale_intensity


# TIRL IMPORTS

from tirl.constants import *
from tirl.tfield import TField
from tirl import settings as ts


# Safe import Matplotlib
if ts.ENABLE_VISUALISATION:
    import os
    import matplotlib
    if os.getenv("DISPLAY"):
        matplotlib.use(ts.MPL_BACKEND)
    else:
        matplotlib.use("agg")
    import matplotlib.pyplot as plt
else:
    import warnings
    warnings.warn("Visualisations are disabled in tirlconfig. "
                  "Matplotlib was not loaded.", ImportWarning)


# IMPLEMENTATION

def preview_composite(obj, **kwargs):
    """
    Preview tensor field as a multi-channel image: each channel is displayed in
    a separate window as a grayscale image.

    :param obj:
        Caller instance.
    :type obj: TField
    :param kwargs:
        "imfunc": applies function on image date immediately before display
        Additional keyword arguments to the preview method.
    :type kwargs: Any

    """
    interrupt = kwargs.pop("interrupt", True)
    data = obj.dataview(TENSOR_MAJOR).reshape(-1, *obj.vshape)
    cmap = kwargs.pop("cmap", "gray")
    imfunc = kwargs.pop("imfunc", lambda x: x)
    for i in range(obj.tsize):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        if obj.vdim == 2:
            ax.imshow(imfunc(data[i]), cmap=cmap, **kwargs)
        elif obj.vdim == 3:
            z = obj.vshape[-1] // 2
            ax.imshow(imfunc(data[i][..., z]), cmap=cmap, **kwargs)
        else:
            raise NotImplementedError("Only 2D or 3D fields can be previewed.")
        ax.set_aspect("equal")
        multiindex = np.unravel_index(i, obj.tshape)
        ax.set_title("T={}".format(multiindex))
        fig.canvas.draw()
        fig.show()
    if interrupt:
        plt.show()
    else:
        fig.show()
    plt.pause(0.01)


def preview_rgb(obj, **kwargs):
    """
    Display tensor field components as RGB channels combined.

    :param obj:
        Caller instance.
    :type obj: TField
    :param kwargs:
        Additional keyword arguments to the preview method.
    :type kwargs: Any

    """
    interrupt = kwargs.pop("interrupt", True)
    imfunc = kwargs.pop("imfunc", lambda x: x)
    if obj.dtype == np.uint8:
        n_channels = reduce(mul, obj.tshape)
        if n_channels in (3, 4):
            data = obj.dataview(VOXEL_MAJOR)
            fig, ax = plt.subplots(1, 1)
            ax.set_aspect("equal")
            ax.imshow(imfunc(data), **kwargs)
            if n_channels == 3:
                ax.set_title("RGB image")
            elif n_channels == 4:
                ax.set_title("RGBA image")
            fig.canvas.draw()
            if interrupt:
                plt.show()
            else:
                fig.show()
            plt.pause(0.01)
            return
        else:
            raise ValueError("RBG display mode is not compatible with tensor "
                             "shape {}.".format(obj.tshape))

    elif np.issubdtype(obj.dtype, np.integer) or \
            np.issubdtype(obj.dtype, np.floating):
        n_channels = reduce(mul, obj.tshape)
        if n_channels in (3, 4):
            data = obj.dataview(TENSOR_MAJOR).reshape(-1, *obj.vshape)
            composite = []
            for i in range(n_channels):
                channel = data[i]
                channel = rescale_intensity(channel, out_range=(0, 1))
                composite.append(channel.astype(np.float32))
            composite = np.stack(composite, axis=-1)
            fig = plt.figure()
            ax = fig.add_subplot(111)
            ax.set_aspect("equal")
            ax.imshow(imfunc(composite), **kwargs)
            ax.set_title("Composite image ({} layers)".format(n_channels))
            fig.canvas.draw()
            if interrupt:
                plt.show()
            else:
                fig.show()
            plt.pause(0.01)
            return
        else:
            raise ValueError("RBG display mode is not compatible with tensor "
                             "shape {}.".format(obj.tshape))
    else:
        raise ValueError("RBG display mode is not compatible with data type "
                         "{}.".format(obj.dtype))


def preview_hsv(obj, **kwargs):
    """
    Visualise the direction and the strength of the tensor field in HSV colour
    coding. Direction is represented by the hue (H), strength is by the
    brightness/value (V).

    :param obj:
        Caller instance.
    :type obj: TField
    :param kwargs:
        Additional keyword arguments to the preview method.
    :type kwargs: Any

    """
    from skimage.color import hsv2rgb
    from tirl.operations.robust import Robust

    imfunc = kwargs.pop("imfunc", None)
    if imfunc is not None:
        warnings.warn("Image function has no effect in HSV mode.")
    interrupt = kwargs.pop("interrupt", True)

    # Initialise output
    hsv = TField(domain=obj.domain, tshape=(3,), order=VOXEL_MAJOR)
    robust = Robust()

    # 1D field
    if obj.tshape == (1,):
        hsv.tensors[2] = rescale_intensity(robust(obj).data)

    # 2D field
    elif obj.tshape == (2,):
        from tirl.operations.tensor import TensorOperator
        field = obj.copy()
        field.dtype = np.complex
        field.tensors[1] = field.tensors[1].data * 1j
        tensorsum = TensorOperator(np.sum)
        values = tensorsum(field, axis=1)
        radii, angles = np.abs(values), np.angle(values)
        hsv.tensors[0] = rescale_intensity(-angles / (2 * np.pi) + 0.5)
        hsv.tensors[1] = rescale_intensity(robust(radii).data)
        hsv.tensors[2] = 1

    # 3D field
    elif obj.tshape == (3,):
        from tirl.operations.tensor import TensorOperator
        norm = TensorOperator(np.linalg.norm, opkwargs=dict(axis=1))
        magnitudes = norm(obj)
        # Set element 1 along tensor axis to imaginary
        field = obj.copy()
        field.dtype = np.complex
        field.tensors[1] = field.tensors[1].data * 1j
        tensorsum = TensorOperator(np.sum, opkwargs=dict(axis=1))
        values = tensorsum(field.tensors[:2])
        radii, angles = np.abs(values), np.angle(values)
        hsv.tensors[0] = rescale_intensity(-angles / (2 * np.pi) + 0.5)
        hsv.tensors[1] = rescale_intensity(robust(radii).data)
        hsv.tensors[2] = rescale_intensity(robust(magnitudes).data)
    else:
        raise ValueError("Tensor shape {} is not compatible with the HSV "
                         "preview mode.".format(obj.tshape))

    rgb = 255 - (255 * hsv2rgb(hsv)).astype(np.uint8)
    frame = kwargs.get("frame", None)
    if frame:
        plt.ion()
        frame.set_data(rgb)
        plt.ioff()
        plt.pause(0.01)
    else:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.imshow(rgb, **kwargs)
        fig.canvas.draw()
        if interrupt:
            plt.show()
        else:
            fig.show()
        plt.pause(0.01)


def preview_quiver(obj, **kwargs):
    """
    Visualise the direction and the strength of the tensor field by arrows
    (quiver plot).

    :param obj:
        Caller instance.
    :type obj: TField
    :param kwargs:
        Additional keyword arguments to the preview method.
    :type kwargs: Any

    """
    imfunc = kwargs.pop("imfunc", None)
    if imfunc is not None:
        warnings.warn("Image function has no effect in quiver mode.")
    interrupt = kwargs.pop("interrupt", True)

    fig = plt.figure()

    _, yp, *zp = np.divide(obj.domain.shape, obj.domain.shape[0])
    xsize = kwargs.pop("xsize", 25)
    ysize = kwargs.pop("ysize", max(int(xsize * yp), 1))
    if zp:
        zp = zp[0]
        zsize = kwargs.pop("zsize", max(int(xsize * zp), 1))
        qobj = obj.resize(xsize, ysize, zsize)
    else:
        qobj = obj.resize(xsize, ysize)

    coords = qobj.domain.get_physical_coordinates()
    coords = coords.transpose()

    vv = qobj.dataview(TENSOR_MAJOR).reshape(qobj.tsize, -1).T
    pv = qobj.domain.map_voxel_vectors(vv, qobj.domain.get_voxel_coordinates())
    u = pv[:, 1]
    v = -pv[:, 0]
    if pv.shape[-1] == 3:
        w = pv[:, 2]
    else:
        w = np.zeros_like(u)
    # u = qobj.tensors[1].data
    # v = -qobj.tensors[0].data  # for inverted y-axis (TIRL convention)

    if qobj.tshape == (3,):
        if coords.shape[0] == 3:
            y, x, z = coords.reshape((3, *qobj.domain.shape))
        elif coords.shape[0] == 2:
            y, x = coords.reshape((2, *qobj.domain.shape))
            z = np.zeros_like(x)
        # w = qobj.tensors[2].data
        ax = fig.gca(projection='3d')
        ax.quiver(x.ravel(), y.ravel(), z.ravel(), u, v, w, **kwargs)
    elif qobj.tshape == (2,):
        y, x = coords.reshape((2, *qobj.domain.shape))
        cmap = kwargs.pop("cmap", None)
        ax = fig.add_subplot(111)
        if cmap is not None:
            color = np.arctan2(x, y) / np.pi + 1
            ax.quiver(x, y, u, v, color, cmap=cmap, **kwargs)
        else:
            ax.quiver(x, y, u, v, **kwargs)
    else:
        raise ValueError("Invalid tensor shape {} for QUIVER mode."
                         .format(qobj.tshape))

    plt.gca().invert_yaxis()
    fig.canvas.draw()
    if interrupt:
        plt.show()
    else:
        fig.show()
    plt.pause(0.01)

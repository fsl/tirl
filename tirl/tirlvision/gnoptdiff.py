#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _       __      __  _         _
#  |__   __| |_   _| |  __ \  | |      \ \    / / (_)       (_)
#     | |      | |   | |__) | | |       \ \  / /   _   ___   _    ___    _ __
#     | |      | |   |  _  /  | |        \ \/ /   | | / __| | |  / _ \  | '_ \
#     | |     _| |_  | | \ \  | |____     \  /    | | \__ \ | | | (_) | | | | |
#     |_|    |_____| |_|  \_\ |______|     \/     |_| |___/ |_|  \___/  |_| |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np


# TIRL IMPORTS

from tirl import settings as ts
from tirl.tirlvision import tfield as tvtf
from skimage.exposure import rescale_intensity

# Safe import Matplotlib
if ts.ENABLE_VISUALISATION:
    import os
    import matplotlib
    if os.getenv("DISPLAY"):
        matplotlib.use(ts.MPL_BACKEND)
    else:
        matplotlib.use("agg")
    import matplotlib.pyplot as plt
else:
    import warnings
    warnings.warn("Visualisations are disabled in tirlconfig. "
                  "Matplotlib was not loaded.", ImportWarning)


# IMPLEMENTATION

def create_object_figure(obj):
    try:
        return obj.figure, True
    except AttributeError:
        obj.figure = plt.figure()
        return obj.figure, False


def show_image(obj, **kwargs):
    interrupt = kwargs.pop("interrupt", True)
    fig, ready = create_object_figure(obj)
    c = obj.costs[0]
    # target = c.filter(c.target)
    # source = c.filter(c.source)
    target = c.target
    source = c.source
    source = source.evaluate(target.domain)
    diff = source - target
    imdata = rescale_intensity(diff.tensors[0].data, out_range=(0, 1))
    if ready:
        plt.ion()
        obj.ax.set_data(imdata)
        plt.ioff()
        plt.pause(0.01)
    else:
        ax = fig.add_subplot(111)
        obj.ax = ax.imshow(imdata, cmap="gray")
        plt.pause(0.01)
        fig.show()


def show_deformation(obj, **kwargs):
    fig, ready = create_object_figure(obj)
    if ready:
        # field = obj.transformation.deformation()
        # shape = obj.transformation.domain.shape
        # field = TField(field.reshape(*shape, -1), taxes=-1, order=VOXEL_MAJOR)
        field = obj.transformation.field()
        tvtf.preview_hsv(field, frame=obj.ax, **kwargs)
    else:
        shape = obj.transformation.domain.shape
        ax = fig.add_subplot(111)
        obj.ax = ax.imshow(np.zeros(shape))
        plt.pause(0.01)
        fig.show()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

from operator import mul
from functools import reduce
from numbers import Integral
from collections.abc import Mapping

# TIRL IMPORTS

from tirl.constants import *
from tirl import settings as ts
from tirl.tirlobject import TIRLObject


# DEFINITIONS


# DEVELOPMENT NOTES


# IMPLEMENTATION

class Layout(TIRLObject, Mapping):

    attrs = ("shape", "tshape", "vshape", "taxes", "vaxes", "order",
             "tdim", "vdim", "ndim", "tsize", "vsize", "size")

    def __init__(self, **kwargs):
        """
        Initialisation of Layout.

        The tensor field layout can be worked out from any of the following
        minimal combination of inputs:

        (shape, taxes), (shape, vaxes)
        (shape, tshape, order), (shape, vshape, order)
        (tshape, vshape, order)
        (tshape, vshape, taxes), (tshape, vshape, vaxes)

        :Keyword Arguments:
            * *vshape* ``tuple`` -- shape of the voxel array
            * *tshape* ``tuple`` -- tensor shape
            * *shape* ``tuple`` -- full shape (both voxel and tensor)
            * *order* ``TENSOR_MAJOR or VOXEL MAJOR`` -- orientation of the
            array axes. If TENSOR_MAJOR, the tensor dimensions are the slowest
            varying (ideal for spatial operations on independent tensor
            components), if VOXEL_MAJOR, the tensor dimensions are the fastest
            varying (ideal for operations that involve computation on whole
            tensors).
            * *taxes* ``tuple`` -- indices of the tensor axes or () for scalar
            fields
            * *vaxes* ``tuple`` -- indices of the the voxel axes

        :raises AssertionError: contradicting or insuffictient inputs

        """
        super(Layout, self).__init__()

        ndim = self._infer_ndim(**kwargs)
        taxes = kwargs.pop("taxes", None)
        vaxes = kwargs.pop("vaxes", None)

        # Initialise configuration
        self.shape = kwargs.pop("shape", None)
        self.tshape = kwargs.pop("tshape", None)
        self.vshape = kwargs.pop("vshape", None)
        self.order = kwargs.pop("order", None)

        # Allow scalar specifications where appropriate
        if isinstance(self.tshape, Integral):
            self.tshape = (self.tshape,)
        if isinstance(taxes, Integral):
            taxes = (taxes,)
        if isinstance(vaxes, Integral):
            vaxes = (vaxes,)

        # Eliminate singleton tensor dimension
        if self.tshape == (1,):
            self.tshape = ()
            taxes = ()

        # Utilise axis definitions (also interpreting negative axes)
        if (taxes is not None) and (ndim is not None):
            self.taxes = tuple(ax % ndim for ax in taxes)
        else:
            self.taxes = taxes
        if (vaxes is not None) and (ndim is not None):
            self.vaxes = tuple(ax % ndim for ax in vaxes)
        else:
            self.vaxes = vaxes

        # Try to work out the full shape first, in case it is not given.
        if self.shape is None:
            assert (self.tshape is not None) and (self.vshape is not None), \
                "Not enough data to infer TField shape."
            if self.order is not None:
                if self.order == TENSOR_MAJOR:
                    self.shape = self.tshape + self.vshape
                elif self.order == VOXEL_MAJOR:
                    self.shape = self.vshape + self.tshape
            elif self.vaxes is not None:
                if self.vaxes[0] == 0:
                    self.shape = self.vshape + self.tshape
                elif self.vaxes[0] == self.tdim:
                    self.shape = self.tshape + self.vshape
                else:
                    raise AssertionError("Invalid field shape configuration.")
            elif self.taxes is not None:
                if self.taxes == ():
                    self.shape = self.vshape
                elif self.taxes[0] == 0:
                    self.shape = self.tshape + self.vshape
                elif self.taxes[0] == self.vdim:
                    self.shape = self.vshape + self.tshape
                else:
                    raise AssertionError("Invalid field shape configuration.")
            else:
                raise AssertionError("Not enough data to infer TField shape.")

        # By this point, we either know the full shape or an error was thrown.
        # So we need to work out the rest of the parameters. It is most
        # straightforward for the cases where either vaxes or taxes is known.
        if self.vaxes is not None:
            if self.vaxes[0] == 0:
                self.order = VOXEL_MAJOR
                self.tshape = self.shape[len(self.vaxes):]
                self.vshape = self.shape[:len(self.vaxes)]
                self.taxes = tuple(range(self.vdim, self.ndim))
            else:
                self.order = TENSOR_MAJOR
                self.tshape = self.shape[:self.ndim - len(self.vaxes)]
                self.vshape = self.shape[self.ndim - len(self.vaxes):]
                self.taxes = tuple(range(self.ndim - self.vdim))

        elif self.taxes is not None:
            if self.taxes == ():
                self.order = self.order or ts.TFIELD_DEFAULT_LAYOUT
                self.tshape = ()
                self.vshape = self.shape
                self.vaxes = tuple(range(self.ndim))
            elif self.taxes[0] == 0:
                self.order = TENSOR_MAJOR
                self.tshape = self.shape[:len(self.taxes)]
                self.vshape = self.shape[len(self.taxes):]
                self.vaxes = tuple(range(self.tdim, self.ndim))
            else:
                self.order = VOXEL_MAJOR
                self.tshape = self.shape[self.ndim - len(self.taxes):]
                self.vshape = self.shape[:self.ndim - len(self.taxes)]
                self.vaxes = tuple(range(self.ndim - self.tdim))

        # Either tshape or vshape, and the order is given.
        elif self.tshape is not None:
            # Infer order from tshape definition and the full shape
            if self.order is None:
                if self.shape[:self.tdim] == self.shape[self.tdim:]:
                    raise AssertionError("Ambiguous shape definition.")
                if self.shape[:self.tdim] == self.tshape:
                    self.order = TENSOR_MAJOR
                elif self.shape[-self.tdim:] == self.tshape:
                    self.order = VOXEL_MAJOR
                else:
                    raise AssertionError("Contradicting shape definitions.")

            if self.order == TENSOR_MAJOR:
                self.vshape = self.shape[self.tdim:]
                if self.tdim > 0:
                    self.taxes = tuple(range(self.tdim))
                else:
                    self.taxes = ()
                self.vaxes = tuple(range(self.tdim, self.ndim))
            elif self.order == VOXEL_MAJOR:
                self.vshape = self.shape[:self.ndim - self.tdim]
                self.taxes = tuple(range(self.vdim, self.ndim))
                self.vaxes = tuple(range(self.vdim))
            else:
                raise AssertionError("TIRL internal error.")

        elif self.vshape is not None:
            # Infer order from vshape definition and the full shape
            if self.order is None:
                if self.shape[:self.vdim] == self.shape[self.vdim:]:
                    raise AssertionError("Ambiguous shape definition.")
                if self.shape[:self.vdim] == self.vshape:
                    self.order = VOXEL_MAJOR
                elif self.shape[-self.vdim:] == self.vshape:
                    self.order = TENSOR_MAJOR
                else:
                    raise AssertionError("Contradicting shape definitions.")

            if self.order == TENSOR_MAJOR:
                self.tshape = self.shape[:self.ndim - self.vdim]
                if self.tdim > 0:
                    self.taxes = tuple(range(self.tdim))
                else:
                    self.taxes = ()
                self.vaxes = tuple(range(self.tdim, self.ndim))
            elif self.order == VOXEL_MAJOR:
                self.tshape = self.shape[self.vdim:]
                self.taxes = tuple(range(self.vdim, self.ndim))
                self.vaxes = tuple(range(self.vdim))
            else:
                raise AssertionError("TIRL internal error.")

        # In all other cases there is (I think) insufficient information to
        # infer the layout.
        else:
            raise AssertionError(
                "Insufficient information to infer layout.")

        # Verify Layout after initialisation
        self.verify()

    def __repr__(self):
        return f"Layout[shape={self.shape}, vshape={self.vshape}, " \
               f"tshape={self.tshape}, vaxes={self.vaxes}, " \
               f"taxes={self.taxes}, order={self.order}]"

    def __layout__(self):
        """
        Layout interface.

        """
        return self

    def __eq__(self, other):
        if isinstance(other, type(self)):
            get = lambda x, key: getattr(x, key)
        elif isinstance(other, dict):
            get = lambda x, key: x.get(key)
        else:
            return False

        for key in self.attrs:
            if getattr(self, key) != get(other, key):
                return False
        else:
            return True

    def __iter__(self):
        for attr in self.attrs:
            yield attr

    def __getitem__(self, item):
        return getattr(self, item)

    def __len__(self):
        return len(self.attrs)

    # PRIVATE METHODS

    def _dump(self):
        objdump = super(Layout, self)._dump()
        objdump.update(
            shape=self.shape,
            tshape=self.tshape,
            vshape=self.vshape,
            taxes=self.taxes,
            vaxes=self.vaxes,
            order=self.order
        )
        return objdump

    @classmethod
    def _load(cls, dump):
        return cls(
            shape=dump["shape"],
            tshape=dump["tshape"],
            vshape=dump["vshape"],
            taxes=dump["taxes"],
            vaxes=dump["vaxes"],
            order=dump["order"]
        )

    # PUBLIC ATTRIBUTES

    @property
    def shape(self):
        return self._shape

    @shape.setter
    def shape(self, s):
        if s is not None:
            assert len(s) > 0, "Null shape."
            self._shape = tuple(int(dim) for dim in s)
        else:
            self._shape = s

    @property
    def tshape(self):
        return self._tshape

    @tshape.setter
    def tshape(self, tsh):
        if tsh is not None:
            if not hasattr(tsh, "__iter__"):
                tsh = (tsh,)
            self._tshape = tuple(int(dim) for dim in tsh)
        else:
            self._tshape = None

    @property
    def vshape(self):
        return self._vshape

    @vshape.setter
    def vshape(self, vsh):
        if vsh is not None:
            if not hasattr(vsh, "__iter__"):
                vsh = (vsh,)
            self._vshape = tuple(int(dim) for dim in vsh)
        else:
            self._vshape = None

    @property
    def taxes(self):
        return self._taxes

    @taxes.setter
    def taxes(self, tax):
        if tax is not None:
            tax = tuple(int(ax) for ax in sorted(tax))
            if len(tax) > 1:
                assert tax == tuple(range(min(tax), max(tax) + 1)), \
                    "Tensor axes must be consecutive integers."
            self._taxes = tuple(tax)
        else:
            self._taxes = None

    @property
    def vaxes(self):
        return self._vaxes

    @vaxes.setter
    def vaxes(self, vax):
        if vax is not None:
            vax = tuple(int(ax) for ax in sorted(vax))
            assert len(vax) > 0, "Invalid voxel axis specification."
            if len(vax) > 1:
                assert vax == tuple(range(min(vax), max(vax) + 1)), \
                    "Voxel axes must be consecutive integers."
            self._vaxes = tuple(vax)
        else:
            self._vaxes = None

    @property
    def order(self):
        return self._order

    @order.setter
    def order(self, o):
        if o is None:
            self._order = None
        elif o in (TENSOR_MAJOR, VOXEL_MAJOR):
            self._order = o
        else:
            raise ValueError("Invalid tensor layout order.")

    @property
    def tdim(self):
        if self.tshape is not None:
            return len(self.tshape)
        else:
            return None

    @property
    def vdim(self):
        if self.vshape is not None:
            return len(self.vshape)
        else:
            return None

    @property
    def ndim(self):
        if self.shape is not None:
            return len(self.shape)
        else:
            return None

    @property
    def numel(self):
        return self.size

    @property
    def size(self):
        return int(reduce(mul, self.shape))

    @property
    def tsize(self):
        if self.tdim > 0:
            return int(reduce(mul, self.tshape))
        else:
            return 1

    @property
    def vsize(self):
        return int(reduce(mul, self.vshape))

    # PUBLIC METHODS

    def copy(self):
        return type(self)(**self)

    def verify(self):
        """
        Performs internal checks to verify the validity of the layout
        configuration.

        """
        # Zeroth-level: types are certified by the getter/setter methods,
        # but they only work if they are all configured.
        for attr in ("shape", "tshape", "vshape", "taxes", "vaxes", "order"):
            assert getattr(self, attr) is not None, f"{attr} is not configured."

        # First-level assertions (orientation independent)
        assert self.tdim == len(self.taxes), "Contradicting tshape and taxes."
        assert self.vdim == len(self.vaxes), "Contradicting vshape and vaxes."
        assert self.ndim == self.vdim + self.tdim, "Contradicting shapes."

        # Second-level assertions (orientation dependent)
        tsh = tuple(dim for ax, dim in enumerate(self.shape)
                    if ax in self.taxes)
        vsh = tuple(dim for ax, dim in enumerate(self.shape)
                    if ax in self.vaxes)

        if self.order == TENSOR_MAJOR:
            assert self.shape == self.tshape + self.vshape, \
                "The sum of tshape and vshape contradicts the full shape."
            assert self.tshape == tsh, "Contradicting tensor axes."
            assert self.vshape == vsh, "Contradicting voxel axes."
        else:
            assert self.shape == self.vshape + self.tshape, \
                "The sum of tshape and vshape contradicts the full shape."
            assert self.tshape == tsh, "Contradicting tensor axes."
            assert self.vshape == vsh, "Contradicting voxel axes."

    def swap(self):
        """ Swaps the order of the tensor and voxel axes. """
        if self.order == TENSOR_MAJOR:
            self.order = VOXEL_MAJOR
            self.vaxes = tuple(range(self.vdim))
            self.taxes = tuple(range(self.vdim, self.ndim))
            self.shape = self.vshape + self.tshape
        else:
            self.order = TENSOR_MAJOR
            self.taxes = tuple(range(self.tdim))
            self.vaxes = tuple(range(self.tdim, self.ndim))
            self.shape = self.tshape + self.vshape

        # Verify new layout
        self.verify()

    @staticmethod
    def _infer_ndim(**kwargs):
        """
        Attempts to find the full dimensionality of the dataset before any of
        shape parameters are set. The purpose is to interpret negative axis
        definitions.

        """
        shape = kwargs.get("shape", None)
        vshape = kwargs.get("vshape", None)
        tshape = kwargs.get("tshape", None)
        ndim = kwargs.get("ndim", None)
        tdim = kwargs.get("tdim", None)
        vdim = kwargs.get("vdim", None)
        taxes = kwargs.get("taxes", None)
        vaxes = kwargs.get("vaxes", None)

        if ndim is not None:
            return ndim
        if shape is not None:
            return len(shape)
        if tdim is None:
            if taxes is not None:
                tdim = len(taxes)
            elif tshape is not None:
                tdim = len(tshape)
        if vdim is None:
            if vaxes is not None:
                vdim = len(vaxes)
            elif vshape is not None:
                vdim = len(vshape)
        if (tdim is not None) and (vdim is not None):
            return vdim + tdim
        else:
            return None

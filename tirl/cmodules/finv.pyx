#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import ctypes
import numpy as np
cimport numpy as np
from scipy.spatial import cKDTree
from tirl.utils import verify_n_cpu
from libc.stdlib cimport malloc, free

from tirl.settings import CPU_CORES
n_cpu = verify_n_cpu(CPU_CORES)
n_cpu = int(max(n_cpu.real, n_cpu.imag))

# 2D functions

cdef extern from "finv2d_src.h":

    int CalculateInverse2D(double** Locations, int* NearestNeighbour, int N,
        int* InputShape, double** Coordinates, double** FwdMappedCoordinates,
        double** InvWarp, int Iterations);

    int CalculateLocalAffine2D(
            double** Locations, int* NearestNeighbour,
            int N, int* InputShape, double** Coordinates,
            double** FwdMappedCoordinates, double** Affine,
            int Iterations);

    int CalculateTxJacobian2D(
            double** Locations, int* NearestNeighbour, int N,
            int* InputShape, double** Coordinates,
            double** Jacobian, int** VertexIndex, int Iterations);

    int FillHoles2D(double** InvWarp, int* Shape, int MaxIter);


# 3D functions

cdef extern from "finv3d_src.h":

    int CalculateInverse3D(
            double** Locations, int* NearestNeighbour, int N, int* InputShape,
            double** Coordinates, double** FwdMappedCoordinates,
            double** InvWarp, int Iterations);

    int CalculateLocalAffine3D(
            double** Locations, int* NearestNeighbour,
            int N, int* InputShape, double** Coordinates,
            double** FwdMappedCoordinates, double** Affine,
            int Iterations);

    int CalculateTxJacobian3D(
            double** Locations, int* NearestNeighbour, int N,
            int* InputShape, double** Coordinates,
            double** Jacobian, int** VertexIndex, int Iterations);

    int FillHoles3D(double** InvWarp, int* Shape, int MaxIter);


# IMPLEMENTATION

def jacobian2d(field, locations, pretx=None, vectorder=(0, 1), maxiter=1000):

    # Obtain the properties of the input field (this must be absolute)
    dim, *forward_shape = field.shape
    cdef np.ndarray[int, ndim=1, mode="c"] fwd_shape = \
        np.asarray(forward_shape, dtype=ctypes.c_int)
    cdef int* fsh = <int *>malloc(2 * sizeof(int))
    fsh = &fwd_shape[0]
    # assert dim == 2, "Input is not a 2D vector field."
    n_fwd_points = np.product(fwd_shape)

    # Obtain locations
    n_points, dim = locations.shape
    cdef np.ndarray[double, ndim=2, mode="c"] c_locations = \
        np.ascontiguousarray(locations, dtype=ctypes.c_double)
    cdef double** loc = <double **>malloc(n_points * sizeof(double *))
    cdef int point;
    for point in range(n_points):
        loc[point] = &c_locations[point, 0]

    # Initialise output array for the Jacobians
    cdef np.ndarray[double, ndim=2, mode="c"] c_jacobians = \
        np.zeros((n_points, 12), dtype=ctypes.c_double)
    cdef double** jac = <double **>malloc(n_points * sizeof(double *))
    for point in range(n_points):
        jac[point] = &c_jacobians[point, 0]

    # Generate output for vertex indices
    cdef np.ndarray[int, ndim=2, mode="c"] c_vertex_index = \
        np.zeros((n_points, 3), dtype=ctypes.c_int)
    cdef int** vi = <int **>malloc(n_points * sizeof(int *))
    for point in range(n_points):
        vi[point] = &c_vertex_index[point, 0]

    # Generate input coordinates of the grid points (physical space)
    fwd_grid_coordinates = np.stack(
            np.mgrid[tuple(slice(ax) for ax in forward_shape)],
            axis=-1).reshape((-1, 2)).astype(ctypes.c_double)
    if callable(pretx):
        fwd_grid_coordinates = pretx(fwd_grid_coordinates)
    cdef np.ndarray[double, ndim=2, mode="c"] c_input_coordinates = \
        np.ascontiguousarray(fwd_grid_coordinates, dtype=ctypes.c_double)
    cdef double** ic = <double **>malloc(n_points * sizeof(double *))
    for point in range(n_fwd_points):
        ic[point] = &c_input_coordinates[point, 0]

    # Find nearest grid point to each location
    tree = cKDTree(fwd_grid_coordinates, leafsize=16, balanced_tree=False,
                   compact_nodes=False)
    cdef np.ndarray[int, ndim=1, mode="c"] nearest_neighbour = \
        np.ascontiguousarray(
            tree.query(locations, k=1, eps=0.1, p=1, workers=n_cpu)[1].ravel(),
            dtype=ctypes.c_int)
    cdef int* nn = <int *>malloc(n_points * sizeof(int))
    nn = &nearest_neighbour[0]

    # Call C function to calculate the local affine for each location
    CalculateTxJacobian2D(loc, nn, n_points, fsh, ic, jac, vi, maxiter)

    # Return results
    tx_jac = np.asarray(c_jacobians, dtype=field.dtype)\
        .reshape((n_points, 2, 6), order="F")
    vindex = np.asanyarray(c_vertex_index).reshape((-1, 3))

    return tx_jac, vindex


def invert(targetlocations, field, gridlocations, vectorder=None, output=None,
           fill_holes=True, maxiter=1000):

    # Set defaults
    n_targetpoints, spacedim = targetlocations.shape
    vectorder = tuple(range(spacedim)) if vectorder is None else vectorder
    cdef int point

    # Create a C pointer to the shape of the input field
    vectdim, *field_shape = field.shape
    voxeldim = len(field_shape)
    n_gridpoints = np.product(field_shape)
    field_shape = tuple(field_shape) + (1,) * (spacedim - voxeldim)
    cdef np.ndarray[int, ndim=1, mode="c"] c_field_shape = \
        np.asarray(field_shape, dtype=ctypes.c_int)

    # Create C pointer to the array of new locations, where the inverse needs
    # to be evaluated
    cdef np.ndarray[double, ndim=2, mode="c"] c_targetlocations = \
        np.ascontiguousarray(targetlocations, dtype=ctypes.c_double)
    cdef double** p_targetlocations = \
        <double **>malloc(n_targetpoints * sizeof(double *))
    for point in range(n_targetpoints):
        p_targetlocations[point] = &c_targetlocations[point, 0]

    # Initialise output array that will store the inverse displacements
    return_output = False
    if output is None:
        output = np.zeros((spacedim, n_targetpoints), dtype=ctypes.c_double)
        fill_holes = False
        return_output = True
    elif isinstance(output, (tuple, list)) and \
         all(isinstance(d, int) for d in output):
        output = np.zeros(shape=(spacedim, *output), dtype=ctypes.c_double)
        return_output = True
    elif hasattr(output, "__array__"):
        output = np.asanyarray(output)
    else:
        raise ValueError(f"Expected array for output, "
                         f"got {output.__class__.__name__} instead.")

    # Create C pointer to output array
    cdef np.ndarray[double, ndim=2, mode="c"] c_output = \
        np.ascontiguousarray(output.reshape(spacedim, -1),
                             dtype=ctypes.c_double)
    cdef double** p_output = <double **>\
        malloc(spacedim * sizeof(double *))
    for point in range(spacedim):
        p_output[point] = &c_output[point, 0]
    backward_shape = output.shape[1:]
    cdef np.ndarray[int, ndim=1, mode="c"] c_backward_shape = \
        np.asarray(backward_shape, dtype=ctypes.c_int)

    # Create C pointer to the array that stores the physical locations of the
    # gridpoints where the forward field is defined exactly.
    cdef np.ndarray[double, ndim=2, mode="c"] c_gridlocations = \
        np.ascontiguousarray(gridlocations, dtype=ctypes.c_double)
    cdef double** p_gridlocations = \
        <double **>malloc(n_gridpoints * sizeof(double *))
    for point in range(n_gridpoints):
        p_gridlocations[point] = &c_gridlocations[point, 0]

    # Generate an array of the physical locations of the transformed grid points
    mapped_gridlocations = gridlocations.copy().astype(field.dtype)
    field = field.reshape((vectdim, -1))
    for i, ax in enumerate(vectorder):
        mapped_gridlocations[:, ax] += field[i]
    cdef np.ndarray[double, ndim=2, mode="c"] c_mapped_gridlocations = \
        np.ascontiguousarray(mapped_gridlocations, dtype=ctypes.c_double)
    cdef double** p_mapped_gridlocations = \
        <double **>malloc(n_gridpoints * sizeof(double *))
    for point in range(n_gridpoints):
        p_mapped_gridlocations[point] = &c_mapped_gridlocations[point, 0]

    # Find the nearest gridpoint for every new location using a k-D tree
    tree = cKDTree(mapped_gridlocations, leafsize=16, balanced_tree=False,
                   compact_nodes=False)
    nearest = tree.query(
        targetlocations, k=1, eps=0.1, p=1, workers=n_cpu)[1].ravel()
    cdef np.ndarray[int, ndim=1, mode="c"] c_nearest = \
        np.ascontiguousarray(nearest, dtype=ctypes.c_int)

    # Choose the appropriate version of the C function
    if spacedim == 2:
        cfunc = CalculateInverse2D
    elif spacedim == 3:
        cfunc = CalculateInverse3D
    else:
        raise NotImplementedError(
            "Only 2D and 3D coordinate mappings can be inverted.")

    # Call the C function
    cfunc(p_targetlocations, &c_nearest[0], n_targetpoints, &c_field_shape[0],
          p_gridlocations, p_mapped_gridlocations, p_output, maxiter)
    if fill_holes:
        if spacedim == 2:
            FillHoles2D(p_output, &c_backward_shape[0], 0)
        elif spacedim == 3:
            FillHoles3D(p_output, &c_backward_shape[0], 0)
        else:
            raise AssertionError("Hole-filling only supports 2D and 3D fields.")

    # Generate output
    if return_output:
        return np.asanyarray(c_output, dtype=field.dtype)
    else:
        output.flat[...] = \
            np.asanyarray(c_output, dtype=output.dtype).ravel()


def local_affine(field, newlocations, gridlocations, output=None,
                 vectorder=None, maxiter=1000):
    """
    Returns the local affine approximation of a field at the given
    physical locations.

    """

    # In the implementation of this function, we aim to pass on the call to a
    # function that is implemented in C for faster performance. The rest of the
    # code is to create pointers to the Python data objects that are required
    # by the C function.

    # Set defaults
    n_newpoints, spacedim = newlocations.shape
    vectorder = tuple(range(spacedim)) if vectorder is None else vectorder
    n_affine = {2: 6, 3: 12}.get(spacedim)
    cdef int point

    # Create a C pointer to the shape of the input field
    vectdim, *field_shape = field.shape
    voxeldim = len(field_shape)
    n_gridpoints = np.product(field_shape)
    field_shape = tuple(field_shape) + (1,) * (spacedim - voxeldim)
    cdef np.ndarray[int, ndim=1, mode="c"] c_field_shape = \
        np.asarray(field_shape, dtype=ctypes.c_int)

    # Create C pointer to the array of new locations, where the affine needs
    # to be evaluated
    cdef np.ndarray[double, ndim=2, mode="c"] c_newlocations = \
        np.ascontiguousarray(newlocations, dtype=ctypes.c_double)
    cdef double** p_newlocations = \
        <double **>malloc(n_newpoints * sizeof(double *))
    for point in range(n_newpoints):
        p_newlocations[point] = &c_newlocations[point, 0]

    # Initialise output array that will store the requested local affines
    return_output = False
    if output is None:
        output = np.zeros((n_newpoints, n_affine), dtype=ctypes.c_double)
        return_output = True
    elif hasattr(output, "__array__"):
        output = np.asanyarray(output)
        assert output.shape == (n_newpoints, n_affine)
    else:
        raise ValueError(f"Expected array for output, "
                         f"got {output.__class__.__name__} instead.")

    # Create C pointer to output array
    cdef np.ndarray[double, ndim=2, mode="c"] c_output = \
        np.ascontiguousarray(output, dtype=ctypes.c_double)
    cdef double** p_output = <double **>malloc(n_newpoints * sizeof(double *))
    for point in range(n_newpoints):
        p_output[point] = &c_output[point, 0]

    # Create C pointer to the array that stores the physical locations of the
    # gridpoints where the field is defined exactly.
    cdef np.ndarray[double, ndim=2, mode="c"] c_gridlocations = \
        np.ascontiguousarray(gridlocations, dtype=ctypes.c_double)
    cdef double** p_gridlocations = \
        <double **>malloc(n_gridpoints * sizeof(double *))
    for point in range(n_gridpoints):
        p_gridlocations[point] = &c_gridlocations[point, 0]

    # Find the nearest gridpoint for every new location using a k-D tree
    tree = cKDTree(gridlocations, leafsize=16, balanced_tree=False,
                   compact_nodes=False)
    nearest = tree.query(
        newlocations, k=1, eps=0.1, p=1, workers=n_cpu)[1].ravel()
    cdef np.ndarray[int, ndim=1, mode="c"] c_nearest = \
        np.ascontiguousarray(nearest, dtype=ctypes.c_int)

    # Generate an array of the physical locations of the transformed grid points
    mapped_gridlocations = gridlocations.copy().astype(field.dtype)
    if vectorder != tuple(range(mapped_gridlocations.shape[-1])):
        for i, ax in enumerate(vectorder):
            mapped_gridlocations[:, ax] += field.reshape((vectdim, -1))[i]
    else:
        mapped_gridlocations[...] += field.reshape((vectdim, -1)).T
    cdef np.ndarray[double, ndim=2, mode="c"] c_mapped_gridlocations = \
        np.ascontiguousarray(mapped_gridlocations, dtype=ctypes.c_double)
    cdef double** p_mapped_gridlocations = \
        <double **>malloc(n_gridpoints * sizeof(double *))
    for point in range(n_gridpoints):
        p_mapped_gridlocations[point] = &c_mapped_gridlocations[point, 0]

    # Choose the appropriate version of the C function
    if spacedim == 2:
        cfunc = CalculateLocalAffine2D
    elif spacedim == 3:
        cfunc = CalculateLocalAffine3D
    else:
        raise NotImplementedError(
            "Only 2D and 3D coordinate spaces are supported.")

    # Call the C function
    cfunc(p_newlocations, &c_nearest[0], n_newpoints, &c_field_shape[0],
          p_gridlocations, p_mapped_gridlocations, p_output, maxiter)

    # Generate output
    if return_output:
        return np.asanyarray(c_output, dtype=field.dtype)
    else:
        output.flat[...] = \
            np.asanyarray(c_output, dtype=output.dtype).ravel()


def jacobian3d(field, locations, pretx=None, vectorder=(0, 1, 2), maxiter=1000):

    # Obtain the properties of the input field (this must be absolute)
    dim, *forward_shape = field.shape
    cdef np.ndarray[int, ndim=1, mode="c"] fwd_shape = \
        np.asarray(forward_shape, dtype=ctypes.c_int)
    cdef int* fsh = <int *>malloc(3 * sizeof(int))
    fsh = &fwd_shape[0]
    # assert dim == 3, "Input is not a 3D vector field."
    n_fwd_points = np.product(fwd_shape)

    # Obtain locations
    n_points, dim = locations.shape
    cdef np.ndarray[double, ndim=2, mode="c"] c_locations = \
        np.ascontiguousarray(locations, dtype=ctypes.c_double)
    cdef double** loc = <double **>malloc(n_points * sizeof(double *))
    cdef int point;
    for point in range(n_points):
        loc[point] = &c_locations[point, 0]

    # Initialise output array for the Jacobians
    cdef np.ndarray[double, ndim=2, mode="c"] c_jacobians = \
        np.zeros((n_points, 36), dtype=ctypes.c_double)
    cdef double** jac = <double **>malloc(n_points * sizeof(double *))
    for point in range(n_points):
        jac[point] = &c_jacobians[point, 0]

    # Generate output for vertex indices
    cdef np.ndarray[int, ndim=2, mode="c"] c_vertex_index = \
        np.zeros((n_points, 4), dtype=ctypes.c_int)
    cdef int** vi = <int **>malloc(n_points * sizeof(int *))
    for point in range(n_points):
        vi[point] = &c_vertex_index[point, 0]

    # Generate input coordinates of the grid points (physical space)
    fwd_grid_coordinates = np.stack(
            np.mgrid[tuple(slice(ax) for ax in forward_shape)],
            axis=-1).reshape((-1, 3)).astype(ctypes.c_double)
    if callable(pretx):
        fwd_grid_coordinates = pretx(fwd_grid_coordinates)
    cdef np.ndarray[double, ndim=2, mode="c"] c_input_coordinates = \
        np.ascontiguousarray(fwd_grid_coordinates, dtype=ctypes.c_double)
    cdef double** ic = <double **>malloc(n_points * sizeof(double *))
    for point in range(n_fwd_points):
        ic[point] = &c_input_coordinates[point, 0]

    # Find nearest grid point to each location
    tree = cKDTree(fwd_grid_coordinates, leafsize=16, balanced_tree=False,
                   compact_nodes=False)
    cdef np.ndarray[int, ndim=1, mode="c"] nearest_neighbour = \
        np.ascontiguousarray(
            tree.query(locations, k=1, eps=0.1, p=1, workers=n_cpu)[1].ravel(),
            dtype=ctypes.c_int)
    cdef int* nn = <int *>malloc(n_points * sizeof(int))
    nn = &nearest_neighbour[0]

    # Call C function to calculate the local affine for each location
    CalculateTxJacobian3D(loc, nn, n_points, fsh, ic, jac, vi, maxiter)

    # Return results
    tx_jac = np.asarray(c_jacobians, dtype=field.dtype)\
        .reshape((n_points, 3, 12), order="F")
    vindex = np.asanyarray(c_vertex_index).reshape((-1, 4))

    return tx_jac, vindex

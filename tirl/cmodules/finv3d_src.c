/*
   _______   _____   _____    _
  |__   __| |_   _| |  __ \  | |
     | |      | |   | |__) | | |
     | |      | |   |  _  /  | |
     | |     _| |_  | | \ \  | |____
     |_|    |_____| |_|  \_\ |______|

 Copyright (C) 2018-2023 University of Oxford
 Part of the FMRIB Software Library (FSL)
 Author: Istvan N. Huszar
 Copyright (C) 2018-2023 University of Oxford */

/*  CCOPYRIGHT  */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cblas.h>
#include <lapacke.h>
#include <limits.h>

#include "finv3d_src.h"


// Basic math

double Dot3D(double v[], double u[]) {
    double Result = 0.0;
    for (int i = 0; i < 3; i++)
        Result += (v[i] * u[i]);
    return Result;
}

int Cross(double v[], double u[], double* Result) {
    Result[0] = v[1]*u[2] - v[2]*u[1];
    Result[1] = v[2]*u[0] - v[0]*u[2];
    Result[2] = v[0]*u[1] - v[1]*u[0];
    return 0;
}

int Difference3D(double v[], double u[], double* Result) {
    for (int i = 0; i < 3; i++) {
        Result[i] = v[i] - u[i];
    }
    return 0;
}

int Unravel3DIndex(int Index, int* Shape, int* MultiIndex) {

    int VoxelCount = Shape[0] * Shape[1] * Shape[2];
    if (Index < VoxelCount) {
        MultiIndex[0] = Index / (Shape[2] * Shape[1]);
        MultiIndex[1] = (Index / Shape[2]) % Shape[1];
        MultiIndex[2] = (Index % Shape[2]);
        return 0;  // Success!
    } else {
        return 1;  // Index is out of bounds!
    }
}

int Ravel3DIndex(int* MultiIndex, int* Shape) {

    int Index = MultiIndex[0] * Shape[1] * Shape[2] + MultiIndex[1] * Shape[2] + MultiIndex[2];
    return Index;
}

int _lin(int* Shape, int i, int j, int k) {

    int Index = i * Shape[1] * Shape[2] + j * Shape[2] + k;
    return Index;
}

lapack_int InvertMat3D(double* A, int N) {
// Performs in-place matrix inversion on A
    int IPIV[N+1];
    lapack_int ret;

    // LU factorisation
    ret = LAPACKE_dgetrf(LAPACK_COL_MAJOR, N, N, A, N, IPIV);
    if (ret != 0)
        return ret;
    // Inverse
    ret = LAPACKE_dgetri(LAPACK_COL_MAJOR, N, A, N, IPIV);
    return ret;
}

// 3D functions

int IsInTetrahedron(double* Point, double** Simplex) {

    int PointInTetrahedron;
    int Vertex, Vertex2, Vertex3, Vertex4;
    int FirstVertex = rand() % 4;
    for (int Run = 0; Run < 4; Run++) {
        Vertex = (FirstVertex + Run) % 4;
        Vertex2 = (Vertex + 1) % 4;
        Vertex3 = (Vertex + 2) % 4;
        Vertex4 = (Vertex + 3) % 4;
        // Find surface normal for the side that is opposite of Vertex1
        double Edge1[3], Edge2[3];
        Difference3D(Simplex[Vertex2], Simplex[Vertex3], Edge1);
        Difference3D(Simplex[Vertex2], Simplex[Vertex4], Edge2);
        double Normal[3];
        Cross(Edge1, Edge2, Normal);
        // Vector from index vertex to point of interest
        double PDist[3];
        Difference3D(Point, Simplex[Vertex2], PDist);
        // Vector from index vertex to secondary vertex
        double VDist[3];
        Difference3D(Simplex[Vertex], Simplex[Vertex2], VDist);
        PointInTetrahedron = ((Dot3D(PDist, Normal) * Dot3D(VDist, Normal)) >= 0) ? -1 : Vertex;
        if (PointInTetrahedron >= 0)
            break;
    }
    return PointInTetrahedron;

}

int InitialiseTetrahedron(int ** Simplex, int* Shape) {

    // Grid coordinates of the tetrahedron vertices
    int _i1 = Simplex[0][0], _j1 = Simplex[0][1], _k1 = Simplex[0][2];
    int _i2 = Simplex[1][0], _j2 = Simplex[1][1], _k2 = Simplex[1][2];
    int _i3 = Simplex[2][0], _j3 = Simplex[2][1], _k3 = Simplex[2][2];
    int _i4 = Simplex[3][0], _j4 = Simplex[3][1], _k4 = Simplex[3][2];

    // Grid shape (for in/out-of-bounds checking)
    int _is = Shape[0], _js = Shape[1], _ks = Shape[2];

    if (_i1 == (_is - 1))
        { _i2 = _i1 - 1; _j2 = _j1; _k2 = _k1; }
    else
        { _i2 = _i1 + 1; _j2 = _j1; _k2 = _k1; }

    if (_j1 == (_js - 1))
        { _i3 = _i1; _j3 = _j1 - 1; _k3 = _k1; }
    else
        { _i3 = _i1; _j3 = _j1 + 1; _k3 = _k1; }

    if (_k1 == (_ks - 1))
        { _i4 = _i1; _j4 = _j1; _k4 = _k1 - 1; }
    else
        { _i4 = _i1; _j4 = _j1; _k4 = _k1 + 1; }

    // Multiplex the fourth vertex if the input is practically 2D
    if (_ks == 1) _k4 = _k1;

    // Update the vertices of the tetrahedron
    Simplex[1][0] = _i2; Simplex[1][1] = _j2; Simplex[1][2] = _k2;
    Simplex[2][0] = _i3; Simplex[2][1] = _j3; Simplex[2][2] = _k3;
    Simplex[3][0] = _i4; Simplex[3][1] = _j4; Simplex[3][2] = _k4;

    return 0;

}

int StepTetrahedron(int** Simplex, int Vertex, int* Shape) {

    int Vertex2 = (Vertex + 1) % 4;
    int Vertex3 = (Vertex + 2) % 4;
    int Vertex4 = (Vertex + 3) % 4;
    int NewPos;
    char flipped = 0;

    // Flipping point about a face that is aligned with the grid
    for (int Axis = 0; Axis < 3; Axis++) {
        if ((Simplex[Vertex2][Axis] == Simplex[Vertex3][Axis]) &&
            (Simplex[Vertex3][Axis] == Simplex[Vertex4][Axis]))
        {
            int denominator = abs(Simplex[Vertex2][Axis] - Simplex[Vertex][Axis]);
            int Step;
            if (denominator == 0) {
                Step = 0;
            } else {
                Step = (Simplex[Vertex2][Axis] - Simplex[Vertex][Axis]) / denominator;
            }
            NewPos = Simplex[Vertex2][Axis] + Step;
            if ((NewPos >= 0) && (NewPos < Shape[Axis])) {
                Simplex[Vertex][Axis] = NewPos;
                return 0;
            } else {
                return 1;
            }
        }
    }

    // Flipping point about a face that does not align with the grid
    if (flipped == 0) {
        for (int Axis = 0; Axis < 3; Axis++) {
            int diff = Simplex[Vertex2][Axis] - Simplex[Vertex][Axis];
            diff += (Simplex[Vertex3][Axis] - Simplex[Vertex][Axis]);
            diff += (Simplex[Vertex4][Axis] - Simplex[Vertex][Axis]);
            diff = abs(diff) > 0 ? diff / abs(diff) : 0;
            NewPos = Simplex[Vertex][Axis] + diff;
            if ((NewPos >= 0) && (NewPos < Shape[Axis]))
                Simplex[Vertex][Axis] = NewPos;

            else
                return 1;
        }
    }

    return 0;

}


int CalculateInverse3D(double** Locations,
    int* NearestNeighbour, int N, int* InputShape,
    double** Coordinates, double** FwdMappedCoordinates,
    double** InvWarp, int Iterations) {

    // Declare simplex to operate on the input grid coordinates
    int** GridSimplex = calloc(4, sizeof(int *));
        for (int Vertex = 0; Vertex < 4; Vertex++)
            GridSimplex[Vertex] = calloc(3, sizeof(int));
    // Declare simplex to operate on the input coordinates
    double** InputSimplex = calloc(4, sizeof(double *));
    // Declare simplex to operate on the forward-mapped coordinates
    double** MappedSimplex = calloc(4, sizeof(double *));

    // Loop over all locations for which the LocalAffine needs to be calculated
    int niter;
    char found_it;
    int err = 0;
    for (int Point = 0; Point < N; Point++) {

        // Initialise GridSimplex with the grid index of the closest of the
        // input points ("NearestNeighbour").
        Unravel3DIndex(NearestNeighbour[Point], InputShape, GridSimplex[0]);
        InitialiseTetrahedron(GridSimplex, InputShape);

        // Get the coordinates of the current target point
        double* TargetPoint = Locations[Point];

        niter = 0;
        found_it = 0;
        while (niter < Iterations) {

            // Create InputSimplex and MappedSimplex from GridSimplex
            for (int Vertex = 0; Vertex < 4; Vertex++) {
                int InputIndex = Ravel3DIndex(GridSimplex[Vertex], InputShape);
                InputSimplex[Vertex] = Coordinates[InputIndex];
                MappedSimplex[Vertex] = FwdMappedCoordinates[InputIndex];
            }

            // Check if the target point is within the input simplex
            int VertexToMirror = IsInTetrahedron(TargetPoint, MappedSimplex);
            if (VertexToMirror >= 0) {
                StepTetrahedron(GridSimplex, VertexToMirror, InputShape);
                niter++;
            } else {
                found_it = 1;
                break;  // Success
            }
        }

        // If the algorithm found the matching simplex, calculate the local
        // inverse displacement by inverting the local affine transformation.
        if (found_it) {
            double LocalAffine[12];
            double TargetSimplex[16], SourceSimplex[16];

            // Populate simplex-coordinate matrices in column-major order
            for (int Vertex = 0; Vertex < 4; Vertex++) {
                SourceSimplex[Vertex * 4 + 0] = MappedSimplex[Vertex][0];
                TargetSimplex[Vertex * 4 + 0] = InputSimplex[Vertex][0];
                SourceSimplex[Vertex * 4 + 1] = MappedSimplex[Vertex][1];
                TargetSimplex[Vertex * 4 + 1] = InputSimplex[Vertex][1];
                SourceSimplex[Vertex * 4 + 2] = MappedSimplex[Vertex][2];
                TargetSimplex[Vertex * 4 + 2] = InputSimplex[Vertex][2];
                SourceSimplex[Vertex * 4 + 3] = 1.0;
                TargetSimplex[Vertex * 4 + 3] = 1.0;
            }

            double sst[16];
            double tst[16];
            for (int row = 0; row < 4; row++) {
                for (int col = 0; col < 4; col++) {
                    sst[row * 4 + col] = SourceSimplex[col * 4 + row];
                    tst[row * 4 + col] = TargetSimplex[col * 4 + row];
                }
            }

            double s[4];
            int rank;
            double rcond = 1.e-8;
            lapack_int invretval = LAPACKE_dgelsd(
                LAPACK_COL_MAJOR, 4, 4, 4, sst, 4, tst, 4, s, rcond, &rank);
            if (invretval == 0) {

                // Populate local inverse affine
                for (int row = 0; row < 3; row++) {
                    for (int col = 0; col < 4; col++) {
                        LocalAffine[col * 3 + row] = tst[row * 4 + col];
                    }
                }

                // Use the local inverse affine to calculate the inverse displacement from the current target point
                double HomoTargetPoint[4] = {TargetPoint[0],
                                             TargetPoint[1],
                                             TargetPoint[2],
                                             1.0};
                double BwdMappedTargetPoint[3] = {0.0, 0.0, 0.0};
                cblas_dgemv(CblasColMajor, CblasNoTrans, 3, 4, 1.0,
                    LocalAffine, 3, HomoTargetPoint, 1, 0.0,
                    BwdMappedTargetPoint, 1);
                InvWarp[0][Point] = BwdMappedTargetPoint[0] - HomoTargetPoint[0];
                InvWarp[1][Point] = BwdMappedTargetPoint[1] - HomoTargetPoint[1];
                InvWarp[2][Point] = BwdMappedTargetPoint[2] - HomoTargetPoint[2];

            } else {
                for (int elem = 0; elem < 3; elem++) {
                    InvWarp[elem][Point] = INFINITY;  // null value
                }
                err++;
            }

        // If the matching simplex could not be found,
        // leave these values undefined and fill them later by local averaging.
        } else {
            for (int elem = 0; elem < 3; elem++) {
                InvWarp[elem][Point] = INFINITY;  // null value
            }
            err++;
        }
    }

    // Free heap pointers
    for (int Vertex = 0; Vertex < 4; Vertex++)
        free(GridSimplex[Vertex]);
    free(GridSimplex);
    free(InputSimplex);
    free(MappedSimplex);

    return err;

}


int CalculateLocalAffine3D(double** Locations,
    int* NearestNeighbour, int N, int* InputShape,
    double** Coordinates, double** FwdMappedCoordinates,
    double** LocalAffine, int Iterations) {

    // Declare simplex to operate on the input grid coordinates
    int** GridSimplex = calloc(4, sizeof(int *));
        for (int Vertex = 0; Vertex < 4; Vertex++)
            GridSimplex[Vertex] = calloc(3, sizeof(int));
    // Declare simplex to operate on the input coordinates
    double** InputSimplex = calloc(4, sizeof(double *));
    // Declare simplex to operate on the forward-mapped coordinates
    double** MappedSimplex = calloc(4, sizeof(double *));

    // Loop over all locations for which the LocalAffine needs to be calculated
    int niter;
    char found_it;
    int err = 0;
    for (int Point = 0; Point < N; Point++) {

        // Initialise GridSimplex with the grid index of the closest of the
        // input points ("NearestNeighbour").
        Unravel3DIndex(NearestNeighbour[Point], InputShape, GridSimplex[0]);
        InitialiseTetrahedron(GridSimplex, InputShape);

        // Get the coordinates of the current target point
        double* TargetPoint = Locations[Point];

        niter = 0;
        found_it = 0;
        while (niter < Iterations) {

            // Create InputSimplex and MappedSimplex from GridSimplex
            for (int Vertex = 0; Vertex < 4; Vertex++) {
                int InputIndex = Ravel3DIndex(GridSimplex[Vertex], InputShape);
                InputSimplex[Vertex] = Coordinates[InputIndex];
                MappedSimplex[Vertex] = FwdMappedCoordinates[InputIndex];
            }

            // Check if the target point is within the input simplex
            int VertexToMirror = IsInTetrahedron(TargetPoint, InputSimplex);
            if (VertexToMirror >= 0) {
                StepTetrahedron(GridSimplex, VertexToMirror, InputShape);
                niter++;
            } else {
                found_it = 1;
                break;  // Success
            }
        }

        // If the algorithm found the matching simplex, calculate the local
        // inverse displacement by inverting the local affine transformation.
        if (found_it) {
            double TargetSimplex[16], SourceSimplex[16];

            // Populate simplex-coordinate matrices in column-major order
            for (int Vertex = 0; Vertex < 4; Vertex++) {
                SourceSimplex[Vertex * 4 + 0] = InputSimplex[Vertex][0];
                TargetSimplex[Vertex * 4 + 0] = MappedSimplex[Vertex][0];
                SourceSimplex[Vertex * 4 + 1] = InputSimplex[Vertex][1];
                TargetSimplex[Vertex * 4 + 1] = MappedSimplex[Vertex][1];
                SourceSimplex[Vertex * 4 + 2] = InputSimplex[Vertex][2];
                TargetSimplex[Vertex * 4 + 2] = MappedSimplex[Vertex][2];
                SourceSimplex[Vertex * 4 + 3] = 1.0;
                TargetSimplex[Vertex * 4 + 3] = 1.0;
            }

            double sst[16];
            double tst[16];
            for (int row = 0; row < 4; row++) {
                for (int col = 0; col < 4; col++) {
                    sst[row * 4 + col] = SourceSimplex[col * 4 + row];
                    tst[row * 4 + col] = TargetSimplex[col * 4 + row];
                }
            }

            // int jpvt[4] = {0, 0, 0, 0};
            // int ipiv[4] = {0, 0, 0, 0};
            double s[4];
            int rank;
            double rcond = 1.e-8;
            // lapack_int invretval = LAPACKE_dgelsy(LAPACK_COL_MAJOR, 4, 4, 4, sst, 4, tst, 4, jpvt, rcond, &rank);
            // lapack_int invretval = LAPACKE_dgesv(LAPACK_COL_MAJOR, 4, 4, sst, 4, ipiv, tst, 4);
            lapack_int invretval = LAPACKE_dgelsd(LAPACK_COL_MAJOR, 4, 4, 4, sst, 4, tst, 4, s, rcond, &rank);

            if (invretval == 0) {
                for (int row = 0; row < 3; row++) {
                    for (int col = 0; col < 4; col++) {
                        LocalAffine[Point][col * 3 + row] = tst[row * 4 + col];
                    }
                }

            } else {
                // printf("Inversion failed.");
                for (int elem = 0; elem < 12; elem++) {
                    LocalAffine[Point][elem] = INFINITY;  // null value
                }
                err++;
            }

        // If the matching simplex could not be found,
        // leave these values undefined and fill them later by local averaging.
        } else {
            // printf("Simplex not found.");
            for (int elem = 0; elem < 12; elem++) {
                LocalAffine[Point][elem] = INFINITY;  // null value
            }
            err++;
        }
    }

    // Free heap pointers
    for (int Vertex = 0; Vertex < 4; Vertex++)
        free(GridSimplex[Vertex]);
    free(GridSimplex);
    free(InputSimplex);
    free(MappedSimplex);

    return err;

}

int CalculateTxJacobian3D(double** Locations, int* NearestNeighbour,
    int N, int* InputShape, double** Coordinates,
    double** Jacobian, int** VertexIndex, int Iterations) {

    // Declare simplex to operate on the input grid coordinates
    int** GridSimplex = calloc(4, sizeof(int *));
        for (int Vertex = 0; Vertex < 4; Vertex++)
            GridSimplex[Vertex] = calloc(3, sizeof(int));
    // Declare simplex to operate on the input coordinates
    double** InputSimplex = calloc(4, sizeof(int *));

    // Loop over all locations for which the Jacobian needs to be calculated
    int niter;
    char found_it;
    int err = 0;
    for (int Point = 0; Point < N; Point++) {

        // Initialise GridSimplex with the grid index of the closest of the
        // input points ("NearestNeighbour").
        Unravel3DIndex(NearestNeighbour[Point], InputShape, GridSimplex[0]);
        InitialiseTetrahedron(GridSimplex, InputShape);

        // Get the coordinates of the current target point
        double* TargetPoint = Locations[Point];

        niter = 0;
        found_it = 0;
        while (niter < Iterations) {

            // Create InputSimplex and MappedSimplex from GridSimplex
            for (int Vertex = 0; Vertex < 4; Vertex++) {
                int InputIndex = Ravel3DIndex(GridSimplex[Vertex], InputShape);
                InputSimplex[Vertex] = Coordinates[InputIndex];
            }

            // Check if the target point is within the input simplex
            int VertexToMirror = IsInTetrahedron(TargetPoint, InputSimplex);
            if (VertexToMirror >= 0) {
                StepTetrahedron(GridSimplex, VertexToMirror, InputShape);
                niter++;
            } else {
                found_it = 1;
                break;  // Success
            }
        }

        if (found_it) {

            // Invert the homogeneous position matrix
            double PositionMatrix[16] =
                {InputSimplex[0][0], InputSimplex[0][1], InputSimplex[0][2], 1.0,
                 InputSimplex[1][0], InputSimplex[1][1], InputSimplex[1][2], 1.0,
                 InputSimplex[2][0], InputSimplex[2][1], InputSimplex[2][2], 1.0,
                 InputSimplex[3][0], InputSimplex[3][1], InputSimplex[3][2], 1.0};
            lapack_int InversionRetVal = InvertMat3D(PositionMatrix, 4);
            double* a = &PositionMatrix[0];
            double x[4] = {TargetPoint[0], TargetPoint[1], TargetPoint[2], 1.0};

            // Calculate the 4x3=12 partial derivatives of the local
            // displacement vector with respect to the orthogonal displacements
            // of the vertices of the enclosing tetrahedron.
            // (p: u_0, u_1, u_2, u_3, v_0, v_1, v_2, v_3, w_0, w_1, w_2, w_3)
            // Where u is vertical displacement, v is horizontal displacement,
            // w is in-depth displacement,
            // and the indices denote the vertex of the enclosing tetrahedron.
            // Note that the vertices may not always be in the order of their
            // linear indices in the transformation grid.

            // p = u_0
            Jacobian[Point][0]  = a[0] * x[0] + a[4] * x[1] + a[8] * x[2] + a[12] * x[3]; // x
            Jacobian[Point][1]  = 0.0;                                     // y
            Jacobian[Point][2]  = 0.0;                                     // z
            // p = u_1
            Jacobian[Point][3]  = a[1] * x[0] + a[5] * x[1] + a[9] * x[2] + a[13] * x[3]; // x
            Jacobian[Point][4]  = 0.0;                                     // y
            Jacobian[Point][5]  = 0.0;                                     // z
            // p = u_2
            Jacobian[Point][6]  = a[2] * x[0] + a[6] * x[1] + a[10] * x[2] + a[14] * x[3]; // x
            Jacobian[Point][7]  = 0.0;                                     // y
            Jacobian[Point][8]  = 0.0;                                     // z
            // p = u_3
            Jacobian[Point][9]  = a[3] * x[0] + a[7] * x[1] + a[11] * x[2] + a[15] * x[3]; // x
            Jacobian[Point][10]  = 0.0;                                     // y
            Jacobian[Point][11]  = 0.0;                                     // z

            // p = v_0
            Jacobian[Point][12]  = 0.0;                                     // x
            Jacobian[Point][13]  = a[0] * x[0] + a[4] * x[1] + a[8] * x[2] + a[12] * x[3]; // y
            Jacobian[Point][14]  = 0.0;                                     // z
            // p = v_1
            Jacobian[Point][15]  = 0.0;                                     // x
            Jacobian[Point][16]  = a[1] * x[0] + a[5] * x[1] + a[9] * x[2] + a[13] * x[3]; // y
            Jacobian[Point][17]  = 0.0;                                     // z
            // p = v_2
            Jacobian[Point][18]  = 0.0;                                     // x
            Jacobian[Point][19]  = a[2] * x[0] + a[6] * x[1] + a[10] * x[2] + a[14] * x[3]; // y
            Jacobian[Point][20]  = 0.0;                                     // z
            // p = v_3
            Jacobian[Point][21]  = 0.0;                                     // x
            Jacobian[Point][22]  = a[3] * x[0] + a[7] * x[1] + a[11] * x[2] + a[15] * x[3]; // y
            Jacobian[Point][23]  = 0.0;                                     // z

            // p = w_0
            Jacobian[Point][24]  = 0.0;                                     // x
            Jacobian[Point][25]  = 0.0;                                     // y
            Jacobian[Point][26]  = a[0] * x[0] + a[4] * x[1] + a[8] * x[2] + a[12] * x[3]; // z
            // p = w_1
            Jacobian[Point][27]  = 0.0;                                     // x
            Jacobian[Point][28]  = 0.0;                                     // y
            Jacobian[Point][29]  = a[1] * x[0] + a[5] * x[1] + a[9] * x[2] + a[13] * x[3]; // z
            // p = w_2
            Jacobian[Point][30]  = 0.0;                                     // x
            Jacobian[Point][31]  = 0.0;                                     // y
            Jacobian[Point][32]  = a[2] * x[0] + a[6] * x[1] + a[10] * x[2] + a[14] * x[3]; // z
            // p = w_3
            Jacobian[Point][33]  = 0.0;                                     // x
            Jacobian[Point][34]  = 0.0;                                     // y
            Jacobian[Point][35]  = a[3] * x[0] + a[7] * x[1] + a[11] * x[2] + a[15] * x[3]; // z

            // Return the linear index of the vertices that were used to define
            // the Jacobian. The indices are returned in the order of their
            // appearance in the transformation Jacobian.
            for (int Vertex = 0; Vertex < 4; Vertex++) {
                VertexIndex[Point][Vertex] =
                    Ravel3DIndex(GridSimplex[Vertex], InputShape);
            }
        }

        // If the matching simplex could not be found,
        // leave these values undefined.
        else {
            for (int i = 0; i < 36; i++) {
                Jacobian[Point][i] = INFINITY;  // null value
            }

            // Fill the vertex indices with -1 (error value)
            for (int Vertex = 0; Vertex < 4; Vertex++) {
                VertexIndex[Point][Vertex] = -1;
            }

            err++;
        }
    }

    // Free heap pointers
    for (int Vertex = 0; Vertex < 4; Vertex++)
        free(GridSimplex[Vertex]);
    free(GridSimplex);
    free(InputSimplex);

    return err;

}

int FillHoles3D(double** InvWarp, int* Shape, int MaxIter) {

    int cnt = INT_MAX;
    int niter = 0;
    if (MaxIter == 0) MaxIter = INT_MAX;
    while ((cnt > 0) && (niter < MaxIter)) {
        cnt = 0;
        niter++;
        for (int dim = 0; dim < 3; dim++) {
            double* ScalarField = InvWarp[dim];
            for (int i = 0; i < Shape[0]; i++) {
                for (int j = 0; j < Shape[1]; j++) {
                    for (int k = 0; k < Shape[2]; k++) {
                        int Index = _lin(Shape, i, j, k);
                        if (isinf(ScalarField[Index])) {
                            double val = 0.0;
                            double sum = 0.0;
                            int n = 0;
                            // Vertical neighbours
                            if (i > 0) {
                                val = ScalarField[_lin(Shape, i - 1, j, k)];
                                if (!isinf(val)) { sum += val; n++; }
                            }
                            if (i < (Shape[0] - 1)) {
                                val = ScalarField[_lin(Shape, i + 1, j, k)];
                                if (!isinf(val)) { sum += val; n++; }
                            }
                            // Horizontal neighbours
                            if (j > 0) {
                                val = ScalarField[_lin(Shape, i, j - 1, k)];
                                if (!isinf(val)) { sum += val; n++; }
                            }
                            if (j < (Shape[1] - 1)) {
                                val = ScalarField[_lin(Shape, i, j + 1, k)];
                                if (!isinf(val)) { sum += val; n++; }
                            }
                            // Neighbours at depth
                            if (k > 0) {
                                val = ScalarField[_lin(Shape, i, j, k - 1)];
                                if (!isinf(val)) { sum += val; n++; }
                            }
                            if (k < (Shape[2] - 1)) {
                                val = ScalarField[_lin(Shape, i, j, k + 1)];
                                if (!isinf(val)) { sum += val; n++; }
                            }
                            // Replace null value with the local average
                            if (n > 0) {
                                ScalarField[Index] = sum / (double)n;
                                cnt++;
                            }
                        }
                    }
                }
            }
        }
    }
    return 0;
}

/*
   _______   _____   _____    _
  |__   __| |_   _| |  __ \  | |
     | |      | |   | |__) | | |
     | |      | |   |  _  /  | |
     | |     _| |_  | | \ \  | |____
     |_|    |_____| |_|  \_\ |______|

 Copyright (C) 2018-2023 University of Oxford
 Part of the FMRIB Software Library (FSL)
 Author: Istvan N. Huszar
 Copyright (C) 2018-2023 University of Oxford */

/*  CCOPYRIGHT  */


#ifndef FINV_FINV2D_H
#define FINV_FINV2D_H

#include <lapacke.h>

// Basic math

double Dot2D(double v[], double u[]);
int Difference2D(double v[], double u[], double* Result);
int DifferenceInt2D(int v[], int u[], int* Result);
int Unravel2DIndex(int Index, int* Shape, int* MultiIndex);
int Ravel2DIndex(int* MultiIndex, int* Shape);
lapack_int InvertMat2D(double* A, int N);

// 2D functions

int IsInTriangle(double* Point, double** Simplex);

int StepTriangle(int** Simplex, int Vertex, int* Shape);

int InitialiseTriangle(int ** Simplex, int* Shape);

int CalculateInverse2D(double** Locations, int* NearestNeighbour, int N,
    int* InputShape, double** Coordinates, double** FwdMappedCoordinates,
    double** InvWarp, int Iterations);

int CalculateLocalAffine2D(double** Locations, int* NearestNeighbour,
    int N, int* InputShape, double** Coordinates,
    double** FwdMappedCoordinates, double** Jacobian, int Iterations);

int CalculateTxJacobian2D(double** Locations, int* NearestNeighbour,
    int N, int* InputShape, double** Coordinates,
    double** Jacobian, int** VertexIndex, int Iterations);

int FillHoles2D(double** InvWarp, int* Shape, int MaxIter);


#endif //FINV_FINV2D_H

#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEVELOPMENT NOTES

"""
23-June-2020 Istvan N Huszar:

This package contains modules written in C/C++. Some TIRL functions depend on
these, such as field inversion. Most of this code has been replaced by
native Python code where it was possible.

The FSLInterpolator class provides an example how TIRL can be extended by
custom-written C functions and Cython. The FSLInterpolator implements plain
vanilla linear interpolation. The use of the SciPyInterpolator is recommended
for more consistent behaviour on the edges of the image.

"""
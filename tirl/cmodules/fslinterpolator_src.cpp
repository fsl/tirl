/*
   _______   _____   _____    _
  |__   __| |_   _| |  __ \  | |
     | |      | |   | |__) | | |
     | |      | |   |  _  /  | |
     | |     _| |_  | | \ \  | |____
     |_|    |_____| |_|  \_\ |______|

 Copyright (C) 2018-2023 University of Oxford
 Part of the FMRIB Software Library (FSL)
 Author: Istvan N. Huszar
 Copyright (C) 2018-2023 University of Oxford */

/*  CCOPYRIGHT  */


#include <cstdlib>
#include <cmath>
#include "fslinterpolator_src.hpp"

using namespace std;

namespace FSLINTERPOLATOR {

    template<typename F>
    F q_tri_interpolation(F v000, F v001, F v010, F v011, F v100,
                          F v101, F v110, F v111,
                          double dx, double dy, double dz) {

        // first-order terms
        F temp1 = (v100 - v000) * (F)dx + v000;
        F temp2 = (v101 - v001) * (F)dx + v001;
        F temp3 = (v110 - v010) * (F)dx + v010;
        F temp4 = (v111 - v011) * (F)dx + v011;
        // second-order terms
        F temp5 = (temp3 - temp1) * (F)dy + temp1;
        F temp6 = (temp4 - temp2) * (F)dy + temp2;
        // final-third order term
        return ((temp6 - temp5) * (F)dz + temp5);
    }


    size_t idx(size_t * shape, size_t x, size_t y, size_t z) {
        return (x * shape[1] + y) * shape[2] + z;
    }


    bool in_neigh_bounds(size_t * shape, long x, long y, long z) {
        return ((x >= 0) && (y >= 0) && (z >= 0) &&
               (x < (long)(shape[0] - 1)) && (y < (long)(shape[1] - 1)) &&
               (z < (long)(shape[2] - 1)));
    }


    template<typename F>
    F interpolate(F * data, size_t * shape, double x, double y, double z) {

        long ix = floor(x);
        long iy = floor(y);
        long iz = floor(z);

        double dx = x - (double)ix;
        double dy = y - (double)iy;
        double dz = z - (double)iz;

        F v000 = 0, v001 = 0, v010 = 0, v011 = 0;
        F v100 = 0, v101 = 0, v110 = 0, v111 = 0;

        if (in_neigh_bounds(shape, ix, iy, iz)) {

            v000 = data[idx(shape, ix, iy, iz)];
            v001 = data[idx(shape, ix, iy, iz + 1)];
            v010 = data[idx(shape, ix, iy + 1, iz)];
            v011 = data[idx(shape, ix, iy + 1, iz + 1)];
            v100 = data[idx(shape, ix + 1, iy, iz)];
            v101 = data[idx(shape, ix + 1, iy, iz + 1)];
            v110 = data[idx(shape, ix + 1, iy + 1, iz)];
            v111 = data[idx(shape, ix + 1, iy + 1, iz + 1)];

            return q_tri_interpolation<F>(
                    v000,v001,v010,v011,v100,v101,v110,v111,dx,dy,dz);
        } else {
            return (F)0.0;
        }
    }

    template<typename F>
    void interpolate_all(F * data, size_t * shape, float * coordinates,
                      size_t n, F * out) {
        double x = 0, y = 0, z = 0;
        for (size_t i = 0; i < n; i++) {
            x = (double)coordinates[i*3];
            y = (double)coordinates[i*3 + 1];
            z = (double)coordinates[i*3 + 2];
            out[i] = interpolate<F>(data, shape, x, y, z);
        }
    }

    // 8 bit
     template void interpolate_all<char>(char * data, size_t * shape,
                            float * coordinates, size_t n, char * out);
     template void interpolate_all<unsigned char>(
        unsigned char * data, size_t * shape, float * coordinates, size_t n,
        unsigned char * out);

    // 16 bit
     template void interpolate_all<short int>(short int * data, size_t * shape,
                            float * coordinates, size_t n, short int * out);
     template void interpolate_all<unsigned short int>(
        unsigned short int * data, size_t * shape, float * coordinates,
        size_t n, unsigned short int * out);

    // 32 bit
     template void interpolate_all<int>(int * data, size_t * shape,
                            float * coordinates, size_t n, int * out);
     template void interpolate_all<unsigned int>(
        unsigned int * data, size_t * shape, float * coordinates, size_t n,
        unsigned int * out);
     template void interpolate_all<float>(float * data, size_t * shape,
                             float * coordinates, size_t n, float * out);

    // 64 bit
     template void interpolate_all<long int>(long int * data, size_t * shape,
                            float * coordinates, size_t n, long int* out);
     template void interpolate_all<unsigned long int>(
        unsigned long int * data, size_t * shape, float * coordinates, size_t n,
        unsigned long int * out);
     template void interpolate_all<double>(double * data, size_t * shape,
                             float * coordinates, size_t n, double * out);

    // 128 bit
     template void interpolate_all<long double>(
        long double * data, size_t * shape, float * coordinates, size_t n,
        long double * out);

}

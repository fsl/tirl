#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

""" Beta function and beta class indicators. """


# DEPENDENCIES

import tirl
from tirl.tirlobject import InstanceCounterMeta


# DEFINITIONS

__version__ = tirl.__version__


# IMPLEMENTATION

def beta_function(func):
    """
    Decorator that indicates when a certain function/method is untested. Warns
    the user that results from the respective function/method might be
    incorrect.

    :param func: untested function or method
    :type func: Callable

    :returns: untested function or method with a warning message
    :rtype: Callable

    """
    import warnings
    from functools import wraps
    @wraps(func)
    def wrapped(*args, **kwargs):
        warnings.warn(
            f"{func.__name__} is a beta function in TIRL {__version__}. "
            f"Use with caution: results might be incorrect.")
        return func(*args, **kwargs)
    return wrapped


class BetaClassMeta(InstanceCounterMeta):
    """
    Metaclass that indicates when a certain class is untested. Warns the user
    that results from the respective class might be incorrect.

    """
    def __new__(cls, name, bases, attrs):
        import warnings
        from functools import wraps
        initfunc = attrs['__init__']
        @wraps(initfunc)
        def wrapped(*args, **kwargs):
            self = args[0]
            warnings.warn(f"{self.__class__.__name__} is a beta class in TIRL "
                f"{__version__}. Use with caution: results might be incorrect.")
            initfunc(*args, **kwargs)
        attrs['__init__'] = wrapped
        return super(BetaClassMeta, cls).__new__(cls, name, bases, attrs)

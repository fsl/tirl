#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  _______   _____   _____    _                                 __   _
#  |__   __| |_   _| |  __ \  | |                              / _| (_)
#     | |      | |   | |__) | | |        ___    ___    _ __   | |_   _    __ _
#     | |      | |   |  _  /  | |       / __|  / _ \  | '_ \  |  _| | |  / _` |
#     | |     _| |_  | | \ \  | |____  | (__  | (_) | | | | | | |   | | | (_| |
#     |_|    |_____| |_|  \_\ |______|  \___|  \___/  |_| |_| |_|   |_|  \__, |
#                                                                         __/ |
#                                                                        |___/
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar

# DESCRIPTION

"""
This file contains library-wide defaults, which can be configured before
installation. After installation, only a subset of these settings can be
overridden by modifying the configurations in the locally installed tirl.yml
file. The path of this user configuration file must be specified here, before
TIRL is installed.

"""

import os
import shutil
from tirl.constants import *
from tirl.userconfig import load_user_variables


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                 Visualisation                                #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
ENABLE_VISUALISATION = True
MPL_BACKEND = "Qt5Agg"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                   Resources                                  #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

TWD = f"{os.getenv('TMPDIR', '/tmp')}/tirl-{os.getuid()}/TWD"
CPU_CORES = 1 + 1j
DEFAULT_FLOAT_TYPE = "f8"
DEFAULT_SHELL = "/bin/bash"
MP_CONTEXT = "fork"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                   Reporting                                  #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
IGNORE_WARNINGS = False
COST_VALUE_LOG_LEVEL = 7
PARAMETER_UPDATE_LOG_LEVEL = 5

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                               TIRLObject class                               #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Default file extensions (must be lower-case)
EXTENSIONS = {
    "Domain"                 : "dom",
    "Chain"                  : "ch",
    "Interpolator"           : "ipol",
    "TField"                 : "tf",
    "TImage"                 : "timg",
    "Transformation"         : "tx",
    "TIRLObject"             : "tobj"
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                             ParameterVector class                            #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
DEFAULT_PARAMETER_DTYPE = "f8"
# Pedantic option. This will halt the program if any transformation parameter
# gets out of bounds. If True, one must make sure that bounds are ALWAYS
# updated with the parameters. Otherwise the program may suddenly terminate
# when it sees new data.
RAISE_BOUNDS_ERROR = True

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                 Domain class                                 #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
DOMAIN_INSTANCE_MEMORY_LIMIT = "2G"    # in MiB   (recommended: at least 2 GiB)
DOMAIN_USE_CACHE = True  # needs RAM

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                              Interpolator class                              #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
FILL_VALUE = 0

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                 TField class                                 #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# Maximum amount of memory that any TImage instance may occupy
TFIELD_INSTANCE_MEMORY_LIMIT = "10G"
CHUNK_THRESHOLD = 225000000  # (15000 x 15000)
# Chunksize for operations on memory-mapped tensor fields
TFIELD_CHUNKSIZE = "10M"
# Data layout schemes
TFIELD_DEFAULT_LAYOUT = VOXEL_MAJOR
# Default interpolation
DEFAULT_COMPACT_INTERPOLATOR = \
    "tirl.interpolation.scipyinterpolator.ScipyInterpolator"
DEFAULT_NONCOMPACT_INTERPOLATOR = \
    "tirl.interpolation.rbfinterpolator.RbfInterpolator"
# Pre-smoothing before resampling: kernel size defined as the number of sigmas
TFIELD_PRESMOOTH = True
TFIELD_PRESMOOTH_KERNELSIZE_NSIGMA = 3
# Ignore singleton dimensions when loading data from array/file
TFIELD_IGNORE_SINGLETON_DIMENSIONS = True
# Autodetect tensor axes when loading data from file
TFIELD_AUTODETECT_TENSOR_AXIS = True
# Do not transform tensors on evaluation by default
DEFAULT_TENSOR_XFM_RULE = None

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                 TImage class                                 #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# # Maximum amount of memory that any TImage instance may occupy
TIMAGE_INSTANCE_MEMORY_LIMIT = "3G"
TIMAGE_MASK_INTERPOLATOR = \
    "tirl.interpolation.scipyinterpolator.ScipyInterpolator"
TIMAGE_MASK_MISSING_DATA = True
TIMAGE_DEFAULT_SNAPSHOT_EXT = "png"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                                  Operations                                  #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
OPERATION_CHUNK_THRESHOLD = 225000000  # (15000 x 15000)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                               User configurations                            #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
CONFIGFILE = f'{os.getenv("HOME")}/.config/tirl.yml'
TSREGISTRY = f'{os.getenv("HOME")}/.config/tirlscripts.yml'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
#                       Config/working dir initialisation                      #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# Bugfix 19 Oct 2022 -- INH
if not os.path.exists(TWD):
    os.makedirs(TWD)

# Initialise TIRL config files if they do not exist
tirldir   = os.path.dirname(__file__)
configdir = os.path.dirname(CONFIGFILE)
if not os.path.isdir(configdir):
    os.makedirs(configdir)

if not os.path.exists(CONFIGFILE):
    template = f"{tirldir}/tirl.yml"
    with open(template, "r") as fp:
        cnf = fp.read()
        cnf = cnf.replace("<TWD>", TWD)
    with open(CONFIGFILE, "w") as fp:
        fp.write(cnf)

if not os.path.exists(TSREGISTRY):
    template = f"{tirldir}/tirlscripts.yml"
    shutil.copy2(template, TSREGISTRY)

globals().update(load_user_variables(CONFIGFILE))

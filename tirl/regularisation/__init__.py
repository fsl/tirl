#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# PACKAGE IMPORTS

from tirl.regularisation.regulariser import Regulariser
# from tirl import expose_package_contents
#
# # Expose all regularisation at the module level
# # Note: this routine imports the Regulariser base class, and every
# # subclass thereof from all modules within the "regularisation" package.
#
# expose_package_contents(baseclass=Regulariser, pkg="tirl.regularisation",
#                         pathology=__path__, globals=globals())

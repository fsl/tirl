#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# TIRL IMPORTS

from tirl.regularisation.regulariser import Regulariser
from tirl.optimisation.optgroup import OptimisationGroup
from tirl.transformations.transformation import Transformation


# IMPLEMENTATION

class TransformationRegulariser(Regulariser):
    """
    TransformationRegulariser - a class for regularisation terms that are
    computed solely from transformation parameters.

    """
    RESERVED_KWARGS = ("transformation", "weight", "logger", "metaparameters")

    def __init__(self, transformation, weight=1.0, logger=None,
                 **metaparameters):
        """
        Initialisation of TransformationRegulariser.

        :param transformation:
            Transformation object whose parameters are used to compute to the
            regularisation term.
        :type transformation: Transformation or OptimisationGroup
        :param weight: Regularisation weight.
        :type weight: Real
        :param logger:
            If None (default), logs will be redirected to the RootLogger
            instance. You may alternatively specify a different Logger instance
            or its name.
        :type logger: None or str or Logger
        :param kwargs:
            Additional keyword arguments required to compute the
            regularisation term.
        :type kwargs: Any

        """
        # Check whether the input has suitable parameters
        if not isinstance(transformation, (Transformation, OptimisationGroup)):
            raise TypeError(
                f"Expected Transformation or OptimisationGroup "
                f"for {self.__class__.__name__}, "
                f"got {transformation.__class__.__name__} instead.")

        # Call superclass initialisation
        super(TransformationRegulariser, self).__init__(
            transformation.parameters, weight=weight, logger=logger,
            **metaparameters)

        # Obtain a hook to the transformation or optimisation group, so that
        # they can be identified by the optimiser.
        self.transformation = transformation

    @property
    def transformation(self):
        return self.metaparameters.get("transformation")

    @transformation.setter
    def transformation(self, tx):
        if isinstance(tx, Transformation):
            self.metaparameters.update(transformation=tx)
        else:
            raise TypeError(f"Expected a Transformation (or subclass), "
                            f"got {tx} instead.")


if __name__ == "__main__":
    print("""This module is not intended for execution.""")

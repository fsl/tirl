#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import os
import numpy as np
from Cython.Build import cythonize
from setuptools import setup, Extension, find_packages

basedir = os.path.dirname(__file__)

#-------------------------------- C/C++ Modules -------------------------------#

define_macros = [
    ('NPY_NO_DEPRECATED_API', 'NPY_1_7_API_VERSION'),
]

# Field inversion (2D and 3D)
fieldinversion = \
    Extension("tirl.cmodules.finv",
              sources=["tirl/cmodules/finv2d_src.c",
                       "tirl/cmodules/finv3d_src.c",
                       "tirl/cmodules/finv.pyx"],
              include_dirs=[np.get_include()],
              extra_link_args=[
                  "-llapacke", "-llapack", "-lcblas", "-lblas", "-lgfortran"],
              extra_compile_args=["-std=c99"],
              define_macros=define_macros,
              language="c")

# FSLInterpolator
# Implements the linear interpolation routine of FLIRT (FSL).
# This is an example of a C++ extension to TIRL.
fslinterpolator = \
    Extension("tirl.cmodules.fslinterpolator",
              sources=["tirl/cmodules/fslinterpolator_src.cpp",
                       "tirl/cmodules/fslinterpolator.pyx"],
              extra_compile_args=["-fpermissive"],
              define_macros=define_macros,
              language="c++")

cython_modules = cythonize([fieldinversion, fslinterpolator],
    compiler_directives={'embedsignature': True, 'language_level': "3"}
)

#------------------Read version number from tirl/__init__.py-------------------#

version = {}
with open(os.path.join(basedir, "tirl", "__init__.py")) as f:
    for line in f:
        if line.startswith('__version__'):
            exec(line, version)
            break
version = version['__version__']


#-------------------------------- TIRL Installer ------------------------------#

setup(
    name="tirl",
    version=version,
    description="Tensor Image Registration Library",
    author="Istvan N. Huszar",
    ext_modules=cython_modules,
    install_requires=None,
    packages=find_packages("."),
    package_dir={"tirl": "tirl"},
    include_package_data=True,
    scripts=["tirl/tirl"]
)

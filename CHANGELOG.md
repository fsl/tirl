# Changelog

### Upcoming

- FSL integration -> replaces TIRL v2.1.3b in FSL 6.0.4
- FSLeyes plugin for TImage files


### 24 Sep 2023 -- v3.6.2

- Allowing the use of callables, such as the print() method, to record runtime information within Optimiser instances.


### 21 Aug 2023 -- v3.6.1

- Bugfix in ImageIOLoader's metadata handling


### 6 Aug 2023 -- v3.6

- Created a distributed namespace system for tirlscripts
- Created a registry for tirlscripts (tirlscripts.yml) independent of tirl.yml


### 23 Nov 2022 -- v3.5

- NIfTI data with Fortran-ordering is now converted to C-order on import


### 30 Oct 2022 -- v3.4.1

- Bugfix: Changing the TImage resolution now also affects the mask resolution 


### 29 Oct 2022 -- v3.4

- Removed OS-level file handles to reduce the number of simultaneously open files
- TIRL files are no longer loaded into memory from file
- TImage: header is copied to the new instance when the TImage is resized


## 20 Oct 2022 -- v3.3

- Added a new method to resample a TImage to a specific resolution


### 19 Oct 2022 -- v3.2.1

- Bugfix: avoided user conflict on TWD when using TIRL on a multi-user computer


### 9 Oct 2022 -- v3.2

- Added option to save/load TIRLFiles as GZipped archives
- Added TIRLLoader class to remove unused temporary uncompressed files at exit


### 9 Oct 2022 -- v3.1.5

- Changed in TField: internal transformations are assigned at construction
- Changed in ResMgr: resolution transformations are assigned even if (1, ...)
- Bugfix: Mask resolution was not set correctly for multilayer TImages
- Bugfix: OptNL metaparameters were not recognised for scalar optimisation
- Bugfix: TImage and TField instance name was not auto-set after re-construction


### 26 Sep 2022 -- v3.1.4

- Added MINCLoader: I/O of MINC-formatted MRI volumes
- Added minc.py: a compatibility layer for linear and non-linear MNI transformations
- Added CostPoins: point square distance metric for landmark-based registration
- Bugfix: prevent floating-point dtype injection in Domain construction

### 20 Sep 2022 -- v3.1.3

- Extended capability for loading VIPS-generated pyramidal TIFF files

### 18 Sep 2022 -- v3.1.2

- Bugfix: large Domain objects no longer return invalid voxel coordinates due to integer-type overflow

### 23 Aug 2022 -- v3.1.1

- Bugfix in loading TIFF tiles
- Binary TImage masks are now saved in 8-bit format to save disk space


### 6 June 2022 -- v3.1

- Added a changelog
- Changed: the tirl main menu
- Changed: tirl scripts are now installed as a separate package
- Changed: tirl config now points to a simplified YAML configuration file instead of settings.py
- Removed data files from the package


### June 2021 -- v3.0

- Rewrote the constructors for most TIRLObjects
- Rewrote the TImage class to support layers and a more intuitive handling of image resolution
- Added support for SharedMemory objects to handle big image/field data in multiprocessing
